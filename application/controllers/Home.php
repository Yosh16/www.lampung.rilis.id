<?php
class Home extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		$this->load->model('T_halamanstatis');
		$this->load->model('T_informasi');
		$this->load->model('T_user');
	}

	// function index($id){
	function index(){

		$data['page'] = 'Home';
		$data['doto'] = $this->T_artikel->recent(3);
		$data['headlines'] = $this->T_artikel->headline();
		$data['pilihan'] = $this->T_artikel->pilihan();
		$data['foto'] = $this->T_artikel->foto();
		$data['video'] = $this->T_artikel->getmenu(141,4,0);
		$data['elektoral'] = $this->T_artikel->getmenu(134,4,0);
		$data['hukum'] = $this->T_artikel->getmenu(136,4,0);
		$data['pemerintah'] = $this->T_artikel->getmenu(137,4,0);
		$data['bisnis'] = $this->T_artikel->getmenu(138,4,0);
		$data['ragam'] = $this->T_artikel->getmenu(139,5,0);
		$data['krakatau'] = $this->T_artikel->getmenu(142,5,0);
		$data['suarakampus'] = $this->T_artikel->kolom(144);
		$data['inspirasi'] = $this->T_artikel->kolom(145);
		$data['opini'] = $this->T_artikel->kolom(146);
		$data['quotes'] = $this->T_artikel->quotes(148);
		$data['populer'] = $this->T_artikel->populer();
		$data['beritalainnya'] =  $this->T_artikel->beritalainnya();
		$data['agenda'] = $this->T_artikel->agenda();
		$data['rilisen'] = $this->T_artikel->sort();
		$data['menu']   = $this->T_halamanstatis->select();
		$data['sosmed']   = $this->T_informasi->get();
		$data['user']  	  			= $this->T_artikel->sort();
		/*$data['rilisen'] = $this->T_artikel->sort();*/
		$this->load->view ('home/index', $data);
	}

	function klm(){

		$data['page'] = 'Home';
		$data['doto'] = $this->T_artikel->recent();
		$data['headlines'] = $this->T_artikel->headline();
		$data['pilihan'] = $this->T_artikel->pilihan();
		$data['foto'] = $this->T_artikel->foto();
		$data['video'] = $this->T_artikel->getmenu(126,4,0);
		$data['elektoral'] = $this->T_artikel->getmenu(16,4,0);
		$data['hukum'] = $this->T_artikel->getmenu(135,4,0);
		$data['pemerintah'] = $this->T_artikel->getmenu(136,4,0);
		$data['bisnis'] = $this->T_artikel->getmenu(14,4,0);
		$data['ragam'] = $this->T_artikel->getmenu(21,5,0);
		$data['krakatau'] = $this->T_artikel->getmenu(141,5,0);
		$data['suarakampus'] = $this->T_artikel->kolom(143);
		$data['inspirasi'] = $this->T_artikel->kolom(144);
		$data['opini'] = $this->T_artikel->kolom(145);
		$data['quotes'] = $this->T_artikel->quotes();
		$data['populer'] = $this->T_artikel->populer();
		$data['beritalainnya'] =  $this->T_artikel->beritalainnya(9);
		$data['agenda'] = $this->T_artikel->agenda();
		$data['rilisen'] = $this->T_artikel->sort();
		$data['menu']   = $this->T_halamanstatis->select();
		$data['sosmed']   = $this->T_informasi->get();
		/*$data['rilisen'] = $this->T_artikel->sort();*/
		$this->load->view ('home/index2', $data);
	}

		function more($j, $cat)
	{
		$a = $this->T_artikel->kategori2($cat);
		$data['lainnya']  = $this->T_artikel->getmenumore($a['id_section'],9,$j);
		$this->load->view('home/more', $data);
	}
}

	?>
