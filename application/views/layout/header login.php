<?php
$base             = $this->config->item('base_url');
$css              = $this->config->item('css');
$images           = $this->config->item('images');
$font             = $this->config->item('font');
$js               = $this->config->item('js');
$fonts            = $this->config->item('fonts');
?>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="keywords" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="author" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta property="og:title" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam"/>
<meta property="og:image" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam"/>
<meta property="og:url" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam"/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>



<meta name="twitter:title" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="twitter:image" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="twitter:url" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="twitter:card" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<link rel="shortcut icon" type="<?php echo $images;?>png" href="image/favicon.png"/>
<link rel="shortcut icon" type="<?php echo $images;?>png" href="image/favicon.png"/>
<link rel="stylesheet" type="<?php echo $css;?>text" href="fonts/font-awesome.min.css"/>
<link rel="stylesheet" href="<?php echo $css;?>bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $css;?>animate.css"/>
<link rel="stylesheet" href="<?php echo $css;?>rilis.css"/>
</head>
<body>