<?php
/**
* 
*/
class T_reaksi_log extends CI_Model
{
	
	protected $_table = 't_reaksi_log';

	function getuser($user, $id)
	{
		$this->db->where('id_user', $user);
		$this->db->where('id_artikel', $id);
		return $this->db->get($this->_table)->row_array();	
	}

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}
}