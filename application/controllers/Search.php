<?php
	class Search extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('T_artikel');
			$this->load->model('T_section');
			$this->load->model('T_informasi');
			$this->load->model('T_halamanstatis');

		}

		function index()
		{
			$data['page'] = 'Search';
			$data['title'] = 'Rilis';
			$data['s'] = $this->input->get('search');
			$data['search'] = $this->T_artikel->search($this->input->get('search'));
			$data['populer'] = $this->T_artikel->populer();
			$data['sosmed'] = $this->T_informasi->get();
			$data['menu']   = $this->T_halamanstatis->select();
			$this->load->view ('search/index', $data);
			
			/*$data['s'] = $this->input->get('search');\
			$data['search'] = $this->T_artikel->search($this->input->get('search'));
			$this->load->view ('search/index', $data);*/
		}
		
		function more($j,$id)
		{
		$data['more']	=	$this->T_artikel->search($j,$id);
		$this->load->view ('search/more', $data);
		}
	}
?>
