<?php
/**
* 
*/
class T_tag extends CI_Model
{
	
	protected $_table = 't_tag';

	function get()
	{
		return $this->db->get($this->_table)->result_array();
	}

	function cekcategory($id)
	    {

	    	$this->db->where('nama_tag',$id);
			$query = $this->db->get($this->_table);
			
			return $query->row_array();
			
		}

	function tag($id)
	 {
	  $this->db->select('*');
	  $this->db->join('t_relasi','t_tag.id_tag = t_relasi.id_object2');
	  $this->db->where('t_relasi.id_object', $id);
	  return $this->db->get($this->_table)->result_array();
	 }
	 
	 function listtt($id)
	{
		return $this->db->get_where('t_tag', array('seo_tag' => $id))->row_array();
	}
}