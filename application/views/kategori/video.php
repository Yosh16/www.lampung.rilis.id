<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<script>
$(document).ready(function(){
    i=0;
    console.log(i);
    $('#more').click(function(){
      var data = $('.form-user3').serialize();
      var cat = $("#catparam").val();
       i = i+4;
       //console.log(i);
       // $('#result').html('');
       j = i;
       console.log(j);
       
       //console.log(data);
       
       $('#more_text').val(j);
        $.ajax({
            url:'<?php echo site_url('kategori/morevideo/');?>'+j+'/'+cat,
            method:'get',
            data:data,
            dataType:'text',
            success:function(data)
            {
              $('#result').append(data);
            }
        });
        
     
    });
  });
</script>
<section id="kategori-page-rilis">
    <div class="container">
        <div class="row col-lg-9-3-kategori-page">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 kategori-page-rilis">
                <div class="row gallery__video__rilis page___home">
                    <?php foreach($headline_vid as $a);
                    if(!empty($a['id_video']));
                    $urlg = $tim.video_image($a['id_video']); ?>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 eight___galery__video__home__rilis">
                        <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>">
                            <div class="post__video__homepage__rilis">
                                <div class="img__post___video"><img src="<?php echo $urlg;?>&w=750&h=500&z=1" alt="video rilis" title="video rilis" class="lazy img100 imgvideorilis" width="750" height="500"></div>
                                <div class="circle__play__rilis"><i class="fa fa-play playme__rilis"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 four___galery__video__home__rilis">
                        <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>">
                            <div class="post__title__video__homepage__rilis">
                                <div class="title__judul__video__big__rilis"><span><?php echo $a['nama_section'];?>:</span> <?php echo $a['judul_artikel'];?></div>
                                <div class="resume__text__video__big__rilis"><?php echo substr(strip_tags($a['isi_artikel']),0,350);?> </div>
                                <div class="post__absolute time_and_date_video__rilis">
                                    <div class="duration__video__big__rilis"></div>
                                    <div class="date__time__video_big_rilis">
                                        <?php
                                        $ex = explode(' ', $a['tgl_pub']);
                                        $num = date('N', strtotime($ex[0]));
                                        $hari = array ( 1 =>    
                                          'Senin',
                                          'Selasa',
                                          'Rabu',
                                          'Kamis',
                                          'Jumat',
                                          'Sabtu',
                                          'Minggu'
                                        );
                                        echo $hari[$num];
                                        ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row gallery__video__rilis__page__home__small">
                     <?php foreach($menu as $a){
                    if(!empty($a['id_video'])){
                    $urlg = $tim.video_image($a['id_video']); ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 gallery__video__rilis__page__home__small">
                        <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>">
                            <div class="post__gallery__video__small">
                                <div class="image__video__small__rilis"><img data-original="<?php echo $urlg;?>&w=750&h=500&z=1" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"></div>
                                <div class="circle__play__rilis_small"><i class="fa fa-play playme__rilis_small"></i></div>
                                <div class="pos__absolute duration__smaller___video">
                                    <?php
                                    $ex = explode(' ', $a['tgl_pub']);
                                    $num = date('N', strtotime($ex[0]));
                                    $hari = array ( 1 =>    
                                      'Senin',
                                      'Selasa',
                                      'Rabu',
                                      'Kamis',
                                      'Jumat',
                                      'Sabtu',
                                      'Minggu'
                                    );
                                    echo $hari[$num];
                                    ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?>
                                </div>
                            </div>
                            <div class="video__title__smaller__rilis text-black"><span><?php echo $a['nama_section'];?> :</span> <?php echo $a['judul_artikel'];?></div>
                        </a>
                    </div>
                    <?php } ;}?>
                </div>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 kategori-page-rilis">
                <div class="widget-title st-2-4">
                    <h3>POPULAR</h3>
                    <div class="separator-6"></div>
                </div>
                <div class="clear"></div>
                <div class="room__berita__popular">
                    <?php
                      $i=1;
                     foreach ($populerberita as $pop){?>
                    <a href="<?php echo site_url(url_title($pop['urltitle']).'.html');?>">
                        <div class="in__room__berita__popular">
                            <div class="pull-left no__populer__rilis"><?php echo $i; ?></div>
                            <div class="pull-left berita__populer__rilis"><?php echo $pop['judul_artikel']; ?></div>
                            <div class="clear"></div>
                        </div>
                    </a>
                    <?php $i++; } ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section id="foto-kategori-terpopuler">
    <div class="container">
        <div class="widget-title st-2-4">
            <h3>VIDEO TERPOPULER</h3>
            <div class="separator-6"></div>
        </div>
        <div class="clear"></div>
        <div class="row">
            <?php 
            foreach($populer as $a){
            if(!empty($a['id_video'])){
            $urlg = $tim.video_image($a['id_video']); ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 gallery__video__rilis__page__home__small">
                <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>">
                    <div class="post__gallery__video__small">
                        <div class="image__video__small__rilis"><img data-original="<?php echo $urlg;?>&w=750&h=500&z=1" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"></div>
                        <div class="circle__play__rilis_small"><i class="fa fa-play playme__rilis_small"></i></div>
                        <div class="pos__absolute duration__smaller___video">
                            <?php
                            $ex = explode(' ', $a['tgl_pub']);
                            $num = date('N', strtotime($ex[0]));
                            $hari = array ( 1 =>    
                              'Senin',
                              'Selasa',
                              'Rabu',
                              'Kamis',
                              'Jumat',
                              'Sabtu',
                              'Minggu'
                            );
                            echo $hari[$num];
                            ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?>
                        </div>
                    </div>
                    <div class="video__title__smaller__rilis populer text-black"><span><?php echo $a['nama_section'];?> :</span> <?php echo $a['judul_artikel'];?></div>
                </a>
            </div>
            <?php } ;}?>
        </div>
    </div>
</section>
<section id="kolom____artikel__rilis__dan__footer">
    <div class="container">
        <div class="row center__berita__pilihan__dan__footer">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col_berita_pilihan_dan_populer_rilis">
                <div class="widget-title st-2-4">
                    <h3>VIDEO LAINNYA</h3>
                    <div class="separator-6"></div>
                </div>
                <div class="clear"></div>
                <div class="banner-klan-middle-01"><img src="<?php echo $images;?>a1462a6f3eb901c258613a43cf3eb3fc.gif" /></div>
                <div class="row" id="result">
                    <?php foreach($terkini_vid as $a){
                    if(!empty($a['id_video'])){
                    $urlg = $tim.video_image($a['id_video']); ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 gallery__video__rilis__page__home__small">
                        <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>">
                            <div class="post__gallery__video__small">
                                <div class="image__video__small__rilis"><img data-original="<?php echo $urlg;?>&w=750&h=500&z=1" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"></div>
                                <div class="circle__play__rilis_small"><i class="fa fa-play playme__rilis_small"></i></div>
                                <div class="pos__absolute duration__smaller___video">
                                    <?php
                                    $ex = explode(' ', $a['tgl_pub']);
                                    $num = date('N', strtotime($ex[0]));
                                    $hari = array ( 1 =>    
                                      'Senin',
                                      'Selasa',
                                      'Rabu',
                                      'Kamis',
                                      'Jumat',
                                      'Sabtu',
                                      'Minggu'
                                    );
                                    echo $hari[$num];
                                    ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?>
                                </div>
                            </div>
                            <div class="video__title__smaller__rilis populer text-black"><span><?php echo $a['nama_section'];?> :</span> <?php echo substr($a['judul_artikel'], 0, 35);?>
                            </div>
                        </a>
                    </div>
                    <?php } ;} ?>
                </div>
                <form class="form-user3">
                <input type="hidden" name="catparam" id="catparam" value="<?php echo $this->uri->segment(2);?>">
                <input type="hidden" name="more_text" id="more_text" value="9">
                </form>
                <div align="center"> 
                <div class="center__more__berita__rilis" id="more">Lihat lagi</div>
          </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col_berita_pilihan_dan_populer_rilis" id="sidebarrightkategori">
            <div class="ads_rilis"><img src="<?php echo $images;?>rec300.jpg" title="ads" class="lazy img100" height="250" width="300"></div>
            <br/>
            <div class="widget-title st-2-4">
              <h3>Rilizen</h3>
              <div class="separator-6"></div>
            </div>
            <div class="clear"></div>
            <br/>
            <div class="kolom__point__rilis">
              <?php foreach($user as $a){ ?>
              <div class="in__kolom__point__rilis"> <a href="http://dev.rilis.id/www.rilis.id/rilizen/user_pointl">
                <div class="pull-left img__point__user"><img data-original="<?php echo $tim.$rilizen.$a['image'];?>" class="lazy img100" alt="poin rilis" title="poin rilis" src="<?php echo $tim.$rilizen.$a['image'];?>" style="display: inline;" height="50" width="50"></div>
                <div class="pull-left name__user__point__rilis">
                  <div><?php echo $a['nama_lengkap'];?></div>
                  <div class="poin__user__box"><?php echo $a['profesi'];?></div>
                </div>
                </a>
                <div class="clear"></div>
              </div>
              <?php } ?>
              <a href="http://dev.rilis.id/www.rilis.id/rilizen/user_pointl">
              <div class="detail-agenda" style="padding-bottom:15px; padding-top:0px;">Lihat selengkapnya....</div>
              </a> </div>
                <div class="tag_footer_div"> 
                      <?php $static = $this->T_halamanstatis->select();
                      $count =  count($static );
                      $i=1;
                      foreach($static as $a){ ?>
                  <a target="_blank" href="<?php echo site_url('page/'.url_title($a['titleurl']));?>">
                    <div class="left_t_footer_n01"><?php echo strtoupper($a['judul_hs']);?></div>
                  </a>
                  <?php } ?>
                
                  <div class="clear"></div>
                </div>
<!--                 <div align="center">
    <div class="apps__center">
        <div class="pull-left apps"><img src="<?php //echo $images;?>google_play.png"></div>
        <div class="pull-left apps store"><img src="<?php //echo $images;?>appstore.png"></div>
        <div class="clear"></div>
    </div>
</div> -->
                <div align="center">
                  <div class="social_media_footer"> <a href="http://<?php echo $sosmed['facebook'];?>" target="_blank">
                    <div class="my_social"><i class="fa fa-facebook fb"></i></div>
                    </a> <a href="http://<?php echo $sosmed['twitter'];?>" target="_blank">
                    <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
                    </a> <a href="http://<?php echo $sosmed['instagram'];?>" target="_blank">
                    <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
                    </a> <a href="http://<?php echo $sosmed['gplus'];?>" target="_blank">
                    <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
                    </a>
                    <div class="clear"></div>
                  </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>