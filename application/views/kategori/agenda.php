<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<script type="text/javascript">
$(document).ready(function() {
    $("#dtp_input1").change(function() {
      var filter = $(data_agenda).val();
      $("#form-date").val(date_agenda);
      $("#btnstatus").click();
      $('#form-date').submit();
    });
  });

/*$(document).ready(function() {
    $("#dtp_input1").onChange(function() {
      var filter = $(data_agenda).val();
      $("#form-date").val(date_agenda);
      $('#form-date').submit();
    });
  });*/
</script>
 <section id="kolom____artikel__rilis__dan__footer">
    <div class="container">
        <div class="row center__berita__pilihan__dan__footer">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col_berita_pilihan_dan_populer_rilis">
                <div class="widget-title st-2-4">
                    <h3>AGENDA GUBERNUR LAMPUNG</h3>
                    <div class="separator-6"></div>
                </div>
                <div class="clear"></div>
                <div class="banner-klan-middle-01"><img src="<?php echo $images; ?>a1462a6f3eb901c258613a43cf3eb3fc.gif" /></div>
                <form action="" class="form-horizontal" name="form-date" id="form-date" role="form" method="post">
                    <div class="pull-right tab_01_date_time">
                        <div class="input-group date form_datetime col-md-12 tab_03_date_time" data-date="<?php echo $date_agenda; ?>" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                            <input class="form-control tab_03_date_time" name="date_agenda" onchange="this.form.submit()" size="16" type="text" value="<?php echo $date_agenda; ?>" readonly>
                            <span class="input-group-addon tab_03_date_time"><span class="glyphicon glyphicon-remove tab_03_date_time"></span></span><span class="input-group-addon tab_03_date_time"><span class="glyphicon glyphicon-calendar tab_03_date_time"></span></span>
                        </div>
                        <input type="hidden" id="dtp_input1" value="<?php echo $date_agenda; ?>" />
                    </div>
                </form>
                <div class="clear"></div>
                <div id="no-more-tables">
                    <table class="table-bordered table-striped table-condensed cf">
                        <thead class="cf add">
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th class="numeric">Waktu</th>
                                <th class="numeric">Tempat</th>
                                <th class="numeric">Acara</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach ($agenda as $agenda) { ?>
                            <tr>
                                <td data-title="No"><?php echo $i;?></td>
                                <td data-title="Tanggal">
                                  <?php echo $date_agenda;?>
                                </td>
                                <td data-title="Waktu" class="numeric"><?php echo $agenda['waktu'];?></td>
                                <td data-title="Tempat" class="numeric"><?php echo $agenda['lokasi'];?></td>
                                <td data-title="Acara" class="numeric"><?php echo $agenda['kegiatan'];?> </td>
                            </tr>
                            <?php ++$i; } ?>
                        </tbody>
                    </table>
                </div>


            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col_berita_pilihan_dan_populer_rilis" id="sidebarrightkategori">
                <div class="widget-title st-2-4">
                    <h3>POPULAR</h3>
                    <div class="separator-6"></div>
                </div>
                <div class="clear"></div>
                <div class="room__berita__popular">
                    <?php $i=1; foreach ($populer as $pop){?>
                      <div class="room__berita__popular"> 
                        <a href="<?php echo site_url(url_title($pop['urltitle']).'.html');?>">
                        <div class="in__room__berita__popular">
                          <div class="pull-left no__populer__rilis"><?php echo $i; ?></div>
                          <div class="pull-left berita__populer__rilis"><?php echo $pop['judul_artikel']; ?></div>
                          <div class="clear"></div>
                        </div>
                        </a>
                      </div>
                    <?php $i++; } ?>
                </div>
                <br/>
                <div class="widget-title st-2-4">
                    <h3>Rilizen</h3>
                    <div class="separator-6"></div>
                </div>
                <div class="clear"></div>
                <br/>
                <div class="kolom__point__rilis">
                    <?php foreach($user as $a){ ?>
                  <div class="in__kolom__point__rilis"> <a href="http://dev.rilis.id/www.rilis.id/rilizen/user_pointl">
                    <div class="pull-left img__point__user"><img data-original="<?php echo $tim.$rilizen.$a['image'];?>" class="lazy img100" alt="poin rilis" title="poin rilis" src="<?php echo $tim.$rilizen.$a['image'];?>" style="display: inline;" height="50" width="50"></div>
                    <div class="pull-left name__user__point__rilis">
                      <div><?php echo $a['nama_lengkap'];?></div>
                      <div class="poin__user__box"><?php echo $a['profesi'];?></div>
                    </div>
                    </a>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <a href="http://dev.rilis.id/www.rilis.id/rilizen/user_pointl">
                  <div class="detail-agenda" style="padding-bottom:15px; padding-top:0px;">Lihat selengkapnya....</div>
                    </a>
                </div>
                <div class="tag_footer_div"> 
              <?php $static = $this->T_halamanstatis->select();
              $count =  count($static );
              $i=1;
              foreach($static as $a){ ?>
          <a target="_blank" href="<?php echo site_url('page/'.url_title($a['titleurl']));?>">
            <div class="left_t_footer_n01"><?php echo strtoupper($a['judul_hs']);?></div>
          </a>
          <?php } ?>
                    <div class="clear"></div>
                </div>
                <!-- <div align="center">
                    <div class="apps__center">
                        <div class="pull-left apps"><img src="image/google_play.png"></div>
                        <div class="pull-left apps store"><img src="image/appstore.png"></div>
                        <div class="clear"></div>
                    </div>
                </div> -->
                <?php 
        $sosmed   = $this->T_informasi->get();?>
          <div class="social_media_footer"> <a href="http://<?php echo $sosmed['facebook'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-facebook fb"></i></div>
            </a> <a href="http://<?php echo $sosmed['twitter'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
            </a> <a href="http://<?php echo $sosmed['instagram'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
            </a> <a href="http://<?php echo $sosmed['gplus'];?>" target="_blank">
            <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>