<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<div class="container" style="display:none;">
  <div class="nm__daerah">
    <div class="pull-left nama_daerah">Nasional</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Lampung</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Bandung</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jawa Tengah</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jogya</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Malang</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jawa Timur</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Makassar</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Papua</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-right nama_daerah">Kota Lainnya <i class="fa fa-angle-down angle-down"></i></div>
    <div class="clear"></div>
  </div>
</div>
<section id="inside-page-rilis">
  <div class="container">
    <div class="row row-inside-page">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 inside-page-col">
        <div class="room-page-inside-rilis"> 
      <?php $static = $this->T_halamanstatis->select();
              $count =  count($static );
              $i=1;
              foreach($static as $a){ 
                if($a['daerah'] == 1) { ?>
                <a href="<?php echo site_url('page/'.$a['titleurl']);?>">
                  <div class="page-in-inside-room"><?php echo strtoupper($a['judul_hs']);?></div>
                </a> 
                <?php } else {'';} ?>
              <?php } ?>
         </div>
      </div>
      <?php if($detailhs['id_hs']!=9){ ?>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 inside-page-col">
        <div class="room__title__kategori-inside"> 
           <a href="#">
          <div class="pull-left left__orange__big"><h1>HALAMAN |</h1></div>
          </a>
          <a href="#">
          <div class="sub__title__menu__kategori pull-left" style="position: relative; top:25px"><h4><?php echo $detailhs['judul_hs'];?></h4></div>
          </a>
          <div class="clear"></div>
        </div>
        <div class="room-text-inside-redaksi-01">
        <div class="img-inside-100"><!-- <img src="<?php //echo $images;?>1505395889-rilis.jpeg"/> --></div>
              <?php echo $detailhs['konten_hs'];?>
        </div>
        
      </div>
      <?php } ?>
      <?php if($detailhs['id_hs']==13){ ?>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 inside-page-col">
        <div class="room-text-inside-redaksi-01">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.0560350048067!2d105.23913323220914!3d-5.408436596077352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNcKwMjQnMzAuNCJTIDEwNcKwMTQnMzYuNiJF!5e0!3m2!1sen!2sid!4v1517727143774" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
           <div class="form-2">
              <div class="pull-left left-pos-form-message">
                  <form action='<?php echo site_url('page/message');?>' method='post'>
                      <div class="input-form-contact-us"><input type="text" name="nama" required placeholder="Nama anda"/></div>
                        <div class="input-form-contact-us"><input type="email" name="email" required placeholder="Email anda"/></div>
                        <div class="input-form-contact-us"><input type="text" name="subject" required placeholder="Subject Pesan"/></div>
                        <div class="input-form-contact-us"><textarea name="pesan" required placeholder="Pesan Anda"></textarea></div>
                        <div align="center">
                          <div class="submitpesan-form2"><button type="submit">Kirim</button></div>
                        </div>
                    </form>
                </div>
                <div class="pull-right pos-detail-address">
                    <p><strong>GRAHA RILIS&nbsp;&nbsp;</strong></p>

                    <p>Jln. Sisingamaraja LK 2 RT 003/ RW 05 <br/> Kel. Kelapa Tiga Permai Kec. Tanjung Karang Barat  <br/> Bandar Lampung - Lampung <br>
                    Telp/Fax. +62-21-84984878/&nbsp;+62-21-788-37665<br>
                    Hotline/Whatsapp: +62 81295593888<br>
                    Email. lampung@rilis.id</p>

                </div>
                <div class="clear"></div>
           </div>
        </div>
        
      </div>
      <?php } ?>
      
      <div class="clear"></div>
    </div>
  </div>
</section>
</body>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>
