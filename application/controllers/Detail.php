<?php
class Detail extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		$this->load->model('T_reaksi');
		$this->load->model('T_komentar');
		$this->load->model('T_halamanstatis');
		$this->load->model('T_informasi');
		$this->load->model('T_user');
		$this->load->model('T_admin');
		$this->load->model('T_tag');
		$this->load->model('T_admin');
	}

	function index($title){
		$data['page'] = 'Detail';
		$data['title'] = 'Rilis';

		$explode = explode('.', $title);

		$id = $explode[0];
		
		$detect = $this->T_artikel->get_test($id);
		if($detect['fokus'] != 0)
		{
			$data['detail']	= $this->T_artikel->get_fokus_detail($id);
		}
		else
		{
			$data['detail'] = $this ->T_artikel->get_artikel($id);
		}
		
		$related = $this ->T_artikel->get_artikel($id);
		$data['detail'] 	 = $this ->T_artikel->get_artikel($id);
		$related 			 = $this ->T_artikel->get_artikel($id);
		$data['terkait'] 	 = $this->T_artikel->related($related['id_section']);
		$data['recent'] 	 = $this->T_artikel->recent(5,0);
		$data['reaksi']	= $this->T_reaksi->getid($id);
		$data['komentar'] = $this->T_komentar->getkomentar($id);
		$data['komentarall'] = $this->T_komentar->getkomentarall($id);
		$data['recent'] = $this->T_artikel->recent(5,0);
		$data['terkait'] = $this->T_artikel->related($related['id_section']);
		$data['tags'] = $this->T_tag->tag($related['id_artikel']);
		$data['populer'] = $this->T_artikel->populer();
		$data['reaksi']	= $this->T_reaksi->getid($id);
		$data['komentar'] = $this->T_komentar->getkomentar($detect['id_artikel']);
		$data['komentarall'] = $this->T_komentar->getkomentarall($detect['id_artikel']);

		// $b = $this ->T_artikel->get_artikel($id);
		// if($b['id_section'] == 139){
		// 	$this->load->view ('detail/detail_foto', $data);
		// }elseif($b['id_section'] == 140){

		$data['sosmed']   = $this->T_informasi->get();
		$a = $this ->T_artikel->get_artikel($id);
		if($a['id_section'] == 140){
			$this->load->view ('detail/detail_foto', $data);
		}elseif($a['id_section'] == 141)
		{
			$this->load->view('detail/detail_video', $data);
		}else{
			$this->load->view('detail/index', $data);
		}
	}
	function foto(){
		$data['page'] = 'Detail/foto';
		$data['title'] = 'Rilis';
		$data['page'] 		 = 'Detail/foto';
		$data['detail'] 	 = $this ->T_artikel->get_artikel($id);
		$related 			 = $this ->T_artikel->get_artikel($id);
		$data['terkait'] 	 = $this->T_artikel->related($related['id_section']);
		$data['recent'] 	 = $this->T_artikel->recent(5,0);
		$data['reaksi']	= $this->T_reaksi->getid($id);
		$data['komentar'] = $this->T_komentar->getkomentar($id);
		$data['komentarall'] = $this->T_komentar->getkomentarall($id);
		$this->load->view ('detail/detail_foto',$data);
	}
	function tag($id)
	{
		$data['page'] = 'List/tag';
		$data['title'] = 'Rilis';
		$data['sosmed']   = $this->T_informasi->get();
		$data['populer'] = $this->T_artikel->populer();
		$data['list']	  = $this->T_tag->listtt($id);
		$data['user']  	  			= $this->T_artikel->sort();
		$data['menu']   = $this->T_halamanstatis->select();
		// $data['headline'] = $this->T_artikel->getmenu($a['id_section'],1,0);
		// $data['menu'] 	  = $this->T_artikel->getmenu($a['id_section'],4,1);
		// $data['lainnya']  = $this->T_artikel->getmenu($a['id_section'],9,5);

		$this->load->view('tag/list', $data);
	}

	function more($j,$id)
		{
		$data['more']	=	$this->T_artikel->list_tag($j,$id);
		$this->load->view ('search/more', $data);
		}
}

?>
