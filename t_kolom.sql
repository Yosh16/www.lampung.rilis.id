-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `t_kolom`;
CREATE TABLE `t_kolom` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `category_id` int(2) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `summary` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `postdate` datetime NOT NULL,
  `oldid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `t_kolom` (`id`, `category_id`, `nama`, `summary`, `image`, `keterangan`, `postdate`, `oldid`) VALUES
(17,	144,	'Fadhil Disaster',	'',	'http://dev.utakatikotak.com/static/2018/01/19/20180119102103Koala.jpg',	'Aku disaster',	'2018-01-15 10:42:16',	0),
(19,	143,	'Fadhil',	'',	'http://dev.utakatikotak.com/static/2018/01/19/20180119102103Koala.jpg',	'Programmer CNF',	'2018-01-16 13:39:38',	0),
(20,	145,	'Rama Mamuaya',	'',	'http://dev.utakatikotak.com/static/2018/01/19/20180119102103Koala.jpg',	'Rama Mamuaya',	'2018-01-19 16:39:38',	0);

-- 2018-01-20 01:39:08
