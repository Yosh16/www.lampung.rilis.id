 <?php include_once dirname(__FILE__).'/../layout/header.php';?>

<div class="container" style="display:none;">
  <div class="nm__daerah">
    <div class="pull-left nama_daerah">Nasional</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Lampung</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Bandung</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jawa Tengah</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jogya</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Malang</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jawa Timur</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Makassar</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Papua</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-right nama_daerah">Kota Lainnya <i class="fa fa-angle-down angle-down"></i></div>
    <div class="clear"></div>
  </div>
</div>
<section id="headline__header">
  <div class="container">
    <div class="row headline__header">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 headline__header">
        <?php 
            $path = date('Y/m/d/', strtotime($quotes['postdate']));
            if(!empty($quotes['thumbnail'])){ 

            $mystring = $quotes['thumbnail'];
            $findme   = 'http';
            $pos = strpos($mystring, $findme);
              if ($pos === false) {?>
        <div class="image_qoute"><img src="<?php echo $tim.$upload.$path.$quotes['thumbnail'];?>&w=531&h=642&zc=1" alt="qoute rilis" title="qoute rilis" class="img100"/></div>
        <?php }else{ ?>
        <div class="image_qoute"><img src="<?php echo $tim.$quotes['thumbnail'];?>&w=531&h=642&zc=1" alt="qoute rilis" title="qoute rilis" class="img100"/></div>
        <?php } ?>
        <?php }else{ ?>
        <div class="image_qoute"><img src="<?php echo $tim.$quotes['oldimage'];?>&w=531&h=642&zc=1" alt="qoute rilis" title="qoute rilis" class="img100"/></div>
        <?php } ?>
      </div>
        <?php 
          $i=1; 
          foreach ($headlines as $hline){
          if ($i==1){
        ?>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 headline__header">
        <div class="headline__news__headers">
          <div class="pull-left big__picture__headline__rilis">
            <div class="pos-relative image__big__headline_rilis"> <a href="detail.html">
               <?php 
            $path = date('Y/m/d/', strtotime($hline['postdate']));
            if(!empty($hline['thumbnail'])){ 

            $mystring = $hline['thumbnail'];
            $findme   = 'http';
            $pos = strpos($mystring, $findme);
              if ($pos === false) {?>
              <div class="ovhidden image______big__headline_rilis"><img data-original="<?php echo $tim.$upload.$path.$hline['thumbnail'];?>&w=531&h=642&zc=0" alt="berita" title="berita" class="lazy img100"/></div>
              <?php }else{ ?>
              <div class="ovhidden image______big__headline_rilis"><img data-original="<?php echo $tim.$hline['thumbnail'];?>&w=531&h=642&zc=0" alt="berita" title="berita" class="lazy img100"/></div>
              <?php } ?>
              <?php }else{ ?>
              <div class="ovhidden image______big__headline_rilis"><img data-original="<?php echo $tim.$hline['oldimage'];?>&w=531&h=642&zc=0" alt="berita" title="berita" class="lazy img100"/></div>
              <?php } ?>
              </a>
              <div class="caption_____big__headline_rilis">
                <div class="top__caption____big__headline_rilis">
                  <div class="pull-left caption____big__headline_rilis date__time">11/11/2017, 13.00 WIB</div>
                  <a href="kategori.html">
                  <div class="pull-right caption____big__headline_rilis tags___caption____big__headline_rilis"><?php echo $hline['nama_section']; ?></div>
                  </a>
                  <div class="clear"></div>
                </div>
                  <a href="kategori.html">
                <div class="judul___berita______big__headline_rilis"><?php echo $hline['judul_artikel']; ?></div>
                </a> </div>
            </div>
          </div>
          <div class="pull-left headline___two__base">
            <?php }else { ?>
            <div class="pos-relative image__headline__two__small rilis">
              <?php 
            $path = date('Y/m/d/', strtotime($hline['postdate']));
            if(!empty($hline['thumbnail'])){ 

            $mystring = $hline['thumbnail'];
            $findme   = 'http';
            $pos = strpos($mystring, $findme);
              if ($pos === false) {?>
              <div class="image_headline__two__small__rilis"><a href="detail.html"><img data-original="<?php echo $tim.$upload.$path.$hline['thumbnail'];?>&w=531&h=642&zc=0" alt="berita" title="berita" class="lazy img100 img__headlinesmall"/></a></div>
              <?php }else{ ?>
              <div class="image_headline__two__small__rilis"><a href="detail.html"><img data-original="<?php echo $tim.$hline['thumbnail'];?>&w=531&h=642&zc=0" alt="berita" title="berita" class="lazy img100 img__headlinesmall"/></a></div>
              <?php } ?>
              <?php }else{ ?>
              <div class="image_headline__two__small__rilis"><a href="detail.html"><img data-original="<?php echo $tim.$hline['oldimage'];?>&w=531&h=642&zc=0" alt="berita" title="berita" class="lazy img100 img__headlinesmall"/></a></div>
              <?php } ?>
              <div class="caption headline__small__two___2">
                <div class="headline__small__two___2__top">
                  <div class="pull-left left__headline__small__two___2__top">11/11/2017, 14.00 WIB</div>
                 <a href="kategori.html"> <div class="pull-right right__headline__small__two___2__top"><?php echo $hline['nama_section']; ?></div></a>
                  <div class="clear"></div>
                </div>
                <a href="detail.html">
                <div class="judul___berita______small__headline_rilis"><?php echo $hline['judul_artikel']; ?></div>
                </a> </div>
            </div>
        <?php }
          $i++;
          } ?>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section id="berita__terkini___">
  <div class="container">
    <div class="row">
      <?php foreach ($doto as $do){?>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box__berita_terikini">
        <div class="frame__berita_terkini__rilis">
          <?php 
            $path = date('Y/m/d/', strtotime($do['postdate']));
            if(!empty($do['thumbnail'])){ 

            $mystring = $do['thumbnail'];
            $findme   = 'http';
            $pos = strpos($mystring, $findme);
              if ($pos === false) {?>
          <div class="pull-left image__berita__terkini"><a href=""><img src="<?php echo $tim.$upload.$path.$do['thumbnail'];?>&w=500&h=262&zc=0" class="lazy image__100" alt="gambar berita rilis" title="gambar berita rilis"  style="display: inline;"></a></div>
          <?php }else{ ?>
          <div class="pull-left image__berita__terkini"><a href=""><img src="<?php echo $tim.$do['thumbnail'];?>&w=500&h=262&zc=0" class="lazy image__100" alt="gambar berita rilis" title="gambar berita rilis"  style="display: inline;"></a></div>
          <?php } ?>
          <?php }else{ ?>
          <div class="pull-left image__berita__terkini"><a href=""><img src="<?php echo $tim.$do['oldimage'];?>&w=500&h=262&zc=0" class="lazy image__100" alt="gambar berita rilis" title="gambar berita rilis"  style="display: inline;"></a></div>
          <?php } ?>
          <div class="pull-left room__berita__terkini">
            <div class="time__berita__terikini"><?php echo ago($do['tgl_pub']);?></div>
            <a href="<?php echo site_url('kategori/'.url_title($do['nama_section']))?>">
            <div class="tagline__berita__terkini"><?php echo $do["nama_section"];?> </div>
            </a>
            <div class="clear"></div>
            <a href="">
            <div class="judul__berita_terkini"><?php echo $do["judul_artikel"];?> </div>
            </a> </div>
          <div class="clear"></div>
        </div>
      </div>
      <?php } ?>
    </div>
    <div></div>
  </div>
</section>
<section id="many__news__populer__pilihan__focus">
  <div class="container">
    <div class="row less">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 news__populer__pilihan__focus">
       <div class="ads-iklan-800"><img src="<?php echo $images;?>iklan-dupont-pioneer-870x725.jpg" alt="iklan" title="iklan"/></div>
        <div class="widget-title st-2-4">
          <h3>POPULAR</h3>
          <div class="separator-6"></div>
        </div>
        <div class="clear"></div>
          <?php
            $i=1;
           foreach ($populer as $pop){?>
        <div class="room__berita__popular"> <a href="">
          <div class="in__room__berita__popular">
            <div class="pull-left no__populer__rilis"><?php echo $i; ?></div>
            <div class="pull-left berita__populer__rilis"><?php echo $pop['judul_artikel']; ?></div>
            <div class="clear"></div>
          </div>
          </a></div>
          <?php $i++; } ?>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 news__populer__pilihan__focus">
        <div class="widget-title st-2-4">
          <h3>BERITA PILIHAN</h3>
          <div class="separator-6"></div>
        </div>
        <div class="clear"></div>
        <div class="room__berita__pilihan">
          <div class="row berita__pilihan__rilis">
            <?php foreach ($pilihan as $pilih) {?>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six__col-lg-6_rilis">
              <div class="pos-relative image__first__rilis__berita__pilihan "> <a href="">
              <?php 
                $path = date('Y/m/d/', strtotime($pilih['postdate']));
                if(!empty($pilih['thumbnail'])){ 

                $mystring = $pilih['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$upload.$path.$pilih['thumbnail'];?>&w=500&h=300&zc=0"></div>
                </a> <a href="">
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$pilih['thumbnail'];?>&w=500&h=300&zc=0"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$pilih['oldimage'];?>&w=500&h=300&zc=0"></div>
                  <?php } ?>
                </a> <a href="">
                <div class="tags___berita____pilihan rilis"><?php echo $pilih['nama_section']; ?></div>
                </a> </div>
              <div class="date__time___berita____pilihan rilis"><?php
                    $ex = explode(' ', $pilih['postdate']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('d/m/Y H.i', strtotime($pilih['postdate']));?></div>
              <a href="">
              <div class="judul____berita____pilihan rilis"><?php echo $pilih['judul_artikel'];?></div>
              <div class="isi____berita____pilihan rilis"><?php echo strip_tags($pilih['isi_artikel']);?></div>
              </a> </div>
              <?php } ?>
            
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section id="video__gallery__rilis">
  <div class="cover__video__gallery__rilis">
    <div class="container">
      <div class="widget-title st-2-4">
        <h3 class="h3fff">Foto Galeri</h3>
        <div class="separator-6"></div>
      </div>
      <div class="clear"></div>
      <div class="row gallery__video__rilis__page__home__small">
        <?php foreach ($foto as $ft){?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 gallery__video__rilis__page__home__small"> <a href="">
          <div class="post__gallery__video__small">
            <?php 
                $path = date('Y/m/d/', strtotime($ft['postdate']));
                echo "<script>console.log(".$path.")</script>";
                if(!empty($ft['thumbnail'])){ 

                $mystring = $ft['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
            <div class="image__video__small__rilis"><img data-original="<?php echo $tim.$upload.$path.$ft['thumbnail'];?>&w=750&h=500&zc=0" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"></div>
            <?php }else{ ?>
            <div class="image__video__small__rilis"><img data-original="<?php echo $tim.$ft['thumbnail'];?>&w=750&h=500&zc=0" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"></div>
            <?php } ?>
            <?php }else{ ?>
            <div class="image__video__small__rilis"><img data-original="<?php echo $tim.$ft['oldimage'];?>&w=750&h=500&zc=0" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"></div>
            <?php } ?>
            <div class="pos__absolute duration__smaller___video"><i class="fa fa-camera camera999"></i><?php echo $ft['dibaca']; ?></div>
          </div>
          <div class="video__title__smaller__rilis"><span><?php echo $ft['nama_section']; ?>:</span><?php echo $ft['judul_artikel']; ?></div>
          </a> </div>
          <?php } ?>
      </div>
    </div>
  </div>
</section>
<section id="kolom____infografis__rilis__wawancara">
  <div class="container">
    <div class="row center__berita__pilihan">
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col_berita_pilihan_dan_populer_rilis custom">
        <div class="row page__kolom__and__infografis">
          <div class="col-lg-6 col-md-6 col-sm-6 col-sm-6 kolom__rilis___home">
            <div class="kolom-berita-per-kategori-rilis-lampung">
              <div class="widget-title st-2-4">
                <h3> ELEKTORAL</h3>
                <div class="separator-6"></div>
              </div>
              <?php 
              $i=1;
              foreach ($elektoral as $etor){
                if ($i==1) {
               ?>
              <div class="clear"></div>
              <div class="in_kolom_rilis kategori">
              <div class="pos-relative image__first__rilis__berita__pilihan "> <a href="">
              <?php 
                $path = date('Y/m/d/', strtotime($etor['postdate']));
                if(!empty($etor['thumbnail'])){ 

                $mystring = $etor['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$upload.$path.$etor['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$etor['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$etor['oldimage'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php } ?>
                  </a> <a href="">
                  <div class="tags___berita____pilihan rilis"><?php echo $etor['nama_section'];?></div>
                  </a> </div>
                <div class="date__time___berita____pilihan rilis"><?php
                    $ex = explode(' ', $etor['postdate']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('d/m/Y H.i', strtotime($etor['postdate']));?></div>
                <div class="judul____berita____pilihan rilis"><?php echo $etor['judul_artikel'];?></div>
                <div class="isi____berita____pilihan rilis"><?php echo $etor['isi_artikel'];?></div>
                <!--in pos kategori--></div>
                <?php }else { ?>
              <div class="kolom___berita____pilihan rilis__002__rilis in-pos-kategori custom">
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis"> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($etor['postdate']));
                if(!empty($etor['thumbnail'])){ 

                $mystring = $etor['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $etor['thumbnail'];?>"" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $etor['thumbnail'];?>"" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $etor['oldimage'];?>"" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  </a> </div>
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs">
                  <div class="pull-left datetime__berita____pilihan rilis__002__rilis">11/11/2017, 14.00 WIB</div>
                  <div class="clear"></div>
                  <div class="judul__berita_pilihan_02_rilis"><?php echo $etor['judul_artikel'];?></div>
                  <div class="tags__berita____pilihan rilis__002__rilis"><?php echo $etor['nama_section'];?></div>
                </div>
                <div class="clear"></div>
              </div>
              <?php }
                  $i++;
               } ?>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-sm-6 kolom__rilis___home">
            <div class="kolom-berita-per-kategori-rilis-lampung">
              <div class="widget-title st-2-4">
                <h3> HUKUM</h3>
                <div class="separator-6"></div>
              </div>
              <?php 
              $i=1;
              foreach ($hukum as $hkm){
                if ($i==1) {
               ?>
              <div class="clear"></div>
              <div class="in_kolom_rilis kategori custom">
                <div class="pos-relative image__first__rilis__berita__pilihan "> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($hkm['postdate']));
                if(!empty($hkm['thumbnail'])){ 

                $mystring = $hkm['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$upload.$path.$hkm['thumbnail'];?>&&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$hkm['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$hkm['oldimage'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php } ?>
                  </a> <a href="">
                  <div class="tags___berita____pilihan rilis"><?php echo $hkm['nama_section']; ?></div>
                  </a> </div>
                <div class="date__time___berita____pilihan rilis">11/11/2017, 17.00 WIB</div>
                <div class="judul____berita____pilihan rilis"><?php echo $hkm['judul_artikel'];?></div>
                <div class="isi____berita____pilihan rilis"><?php echo $hkm['isi_artikel']; ?></div>
                <!--in pos kategori--></div>
                <?php }else{ ?>
              <div class="kolom___berita____pilihan rilis__002__rilis in-pos-kategori custom">
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis"> <a href="">
                   <?php 
                $path = date('Y/m/d/', strtotime($hkm['postdate']));
                if(!empty($hkm['thumbnail'])){ 

                $mystring = $hkm['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $upload.$path.$hkm['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $hkm['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{  ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $hkm['oldimage'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  </a> </div>
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs">
                  <div class="pull-left datetime__berita____pilihan rilis__002__rilis">11/11/2017, 14.00 WIB</div>
                  <div class="clear"></div>
                  <div class="judul__berita_pilihan_02_rilis"><?php echo $hkm["judul_artikel"];?></div>
                  <div class="tags__berita____pilihan rilis__002__rilis"><?php echo $hkm['nama_section']; ?></div>
                </div>
                <div class="clear"></div>
              </div>
              <?php 
                }$i++;
              }
               ?>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-sm-6 kolom__rilis___home">
            <div class="kolom-berita-per-kategori-rilis-lampung">
              <div class="widget-title st-2-4">
                <h3> PEMERINTAHAN</h3>
                <div class="separator-6"></div>
              </div>
              <?php 
              $i=1;
              foreach ($pemerintah as $pmh){
                if ($i==1) {
               ?>
              <div class="clear"></div>
              <div class="in_kolom_rilis kategori custom">
                <div class="pos-relative image__first__rilis__berita__pilihan "> <a href="">
                  <?php 
                $path = date('Y/m/d/', strtotime($pmh['postdate']));
                if(!empty($pmh['thumbnail'])){ 

                $mystring = $pmh['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$upload.$path.$pm['thumbnail']; ?>&&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php }else{ ?>
                <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$pmh['thumbnail'];?>&&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                <?php } ?>
                <?php }else{ ?>
                <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$pmh ['oldimage']; ?>&&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                <?php } ?>
                  </a> <a href="">
                  <div class="tags___berita____pilihan rilis"><?php echo $pmh['nama_section']; ?></div>
                  </a> </div>
                <div class="date__time___berita____pilihan rilis">11/11/2017, 17.00 WIB</div>
                <div class="judul____berita____pilihan rilis"><?php echo $pmh['judul_artikel'];?></div>
                <div class="isi____berita____pilihan rilis"><?php echo $pmh['isi_artikel']; ?></div>
                <!--in pos kategori--></div>
                <?php }else{ ?>
              <div class="kolom___berita____pilihan rilis__002__rilis in-pos-kategori custom">
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis"> <a href="">
                  <?php 
                $path = date('Y/m/d/', strtotime($pmh['postdate']));
                if(!empty($pmh['thumbnail'])){ 

                $mystring = $pmh['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $tim.$upload.$path.$pmh['thumbnail'];?>" class="lazy img100" title="berita" alt="berita" style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $pmh['thumbnail'];?>" class="lazy img100" title="berita" alt="berita" style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $pmh['oldimage'];?>" class="lazy img100" title="berita" alt="berita" style="display: inline;"></div>
                  <?php } ?>
                  </a> </div>
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs">
                  <div class="pull-left datetime__berita____pilihan rilis__002__rilis">11/11/2017, 14.00 WIB</div>
                  <div class="clear"></div>
                  <div class="judul__berita_pilihan_02_rilis"><?php echo $pmh['judul_artikel']; ?></div>
                  <div class="tags__berita____pilihan rilis__002__rilis"><?php echo $pmh['nama_section']; ?></div>
                </div>
                <div class="clear"></div>
              </div>
              <?php 
                }$i++;
              }
              ?>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-sm-6 kolom__rilis___home">
            <div class="kolom-berita-per-kategori-rilis-lampung">
              <div class="widget-title st-2-4">
                <h3> BISNIS</h3>
                <div class="separator-6"></div>
              </div>
              <?php 
              $i=1;
              foreach ($bisnis as $bsn){
                if ($i==1) {
               ?>
              <div class="clear"></div>
              <div class="in_kolom_rilis kategori custom">
                <div class="pos-relative image__first__rilis__berita__pilihan "> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($bsn['postdate']));
                if(!empty($bsn['thumbnail'])){ 

                $mystring = $bsn['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$upload.$path.$bsn['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$bsn['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="image______berita____pilihan rilis"><img data-original="<?php echo $tim.$bsn['oldimage'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                  <?php } ?>
                  </a> <a href="">
                  <div class="tags___berita____pilihan rilis"><?php echo $bsn['nama_section'];?></div>
                  </a> </div>
                <div class="date__time___berita____pilihan rilis">11/11/2017, 17.00 WIB</div>
                <div class="judul____berita____pilihan rilis"><?php echo $bsn['judul_artikel']; ?></div>
                <div class="isi____berita____pilihan rilis"><?php echo $bsn['isi_artikel']; ?></div>
                <!--in pos kategori--></div>
                <?php }else{ ?>
              <div class="kolom___berita____pilihan rilis__002__rilis in-pos-kategori custom">
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis"> <a href="">
                 <?php 
                $path = date('Y/m/d/', strtotime($bsn['postdate']));
                if(!empty($bsn['thumbnail'])){ 

                $mystring = $bsn['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $tim.$upload.$path.$bsn['thumbnail']; ?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $bsn['thumbnail']; ?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis"><img data-original="<?php echo $bsn['oldimage']; ?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  </a> </div>
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs">
                  <div class="pull-left datetime__berita____pilihan rilis__002__rilis">11/11/2017, 14.00 WIB</div>
                  <div class="clear"></div>
                  <div class="judul__berita_pilihan_02_rilis"><?php echo $bsn['judul_artikel']; ?></div>
                  <div class="tags__berita____pilihan rilis__002__rilis"><?php echo $bsn['nama_section']; ?></div>
                </div>
                <div class="clear"></div>
              </div>
              <?php } $i++; } ?>
            </div>
          </div>
          
        </div>
        <div class="clear"></div>
      </div>
      
     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col_berita_pilihan_dan_populer_rilis custom">
      <div class="iklan-page-01"><img src="<?php echo $images;?>iklan-karier-rilislampung.jpg"/></div>
        
        <div class="widget-title st-2-4">
                <h3> AGENDA</h3>
                <div class="separator-6"></div>
              </div>
        
        <?php
          $i=1; 
        foreach ($agenda as $agen){ ?>
      <div id="no-more-tables">
          <table class="table-bordered table-striped table-condensed cf">
            <thead class="cf">
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th class="numeric">Waktu</th>
                <th class="numeric">Tempat</th>
                <th class="numeric">Acara</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-title="No"><?php echo $i; ?></td>
                <td data-title="Tanggal"><?php echo $agen['tanggal']; ?></td>
                <td data-title="Waktu" class="numeric"><?php echo $agen['waktu']; ?></td>
                <td data-title="Tempat" class="numeric"><?php echo $agen['tempat']; ?> </td>
                <td data-title="Acara" class="numeric"><?php echo $agen['acara']; ?></td>
              </tr>
            </tbody>
          </table>
        </div>
          <?php
          $i++;
           } ?>
<a href="detail-agenda.html">
        <div class="detail-agenda">Lihat selengkapnya....</div>
        </a> </div>
      <div class="clear"></div>
     </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<br/>
<div class="ads__top" style="background:url(<?php echo $images;?>030117-oppo-r9s-leeminho-02.jpg);"></div>
<br/>
<section id="kolom____infografis__rilis__wawancara">
  <div class="container">
    <div class="row center__berita__pilihan">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_berita_pilihan_dan_populer_rilis">
        <div class="row page__kolom__and__infografis">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 news__populer__pilihan__focus">
        <div class="widget-title st-2-4">
          <h3>RAGAM</h3>
          <div class="separator-6"></div>
        </div>
          <?php 
              $i=1; 
              foreach ($ragam as $rgm){
                if ($i==1){
                ?>
        <div class="clear"></div>
        <div class="room__berita__pilihan">
          <div class="row berita__pilihan__rilis">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six__col-lg-6_rilis smaller-page2">
              <div class="pos-relative image__first__rilis__berita__pilihan smaller-page2"> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($rgm['postdate']));
                if(!empty($rgm['thumbnail'])){ 

                $mystring = $rgm['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                <div class="image______berita____pilihan rilis smaller-page2"><img data-original="<?php echo $tim.$upload.$path.$rgm['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100"  style="display: inline;"></div>
                <?php }else{ ?>
                <div class="image______berita____pilihan rilis smaller-page2"><img data-original="<?php echo $tim.$rgm['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100"  style="display: inline;"></div>
                <?php } ?>
                <?php }else{ ?>
                <div class="image______berita____pilihan rilis smaller-page2"><img data-original="<?php echo $tim.$rgm['oldimage'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100"  style="display: inline;"></div>
                <?php } ?>
                </a> <a href="">
                <div class="tags___berita____pilihan rilis smaller-page2"><?php echo $rgm['nama_section'];?></div>
                </a> </div>
              <div class="date__time___berita____pilihan rilis smaller-page2">11/11/2017, 17.00 WIB</div>
              <a href="">
              <div class="judul____berita____pilihan rilis smaller-page2"><?php echo $rgm['judul_artikel']; ?></div>
              <div class="isi____berita____pilihan rilis smaller-page2"><?php echo $rgm['isi_artikel'] ?></div>
              </a> </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six__col-lg-6_rilis smaller-page2">
            <?php }else { ?>
              <div class="kolom___berita____pilihan rilis__002__rilis smaller-page2">
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis smaller-page2"> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($rgm['postdate']));
                if(!empty($rgm['thumbnail'])){ 

                $mystring = $rgm['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis smaller-page2"><img data-original="<?php echo $rgm.$path.$upload['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis smaller-page2"><img data-original="<?php echo $rgm['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis smaller-page2"><img data-original="<?php echo $rgm['oldimage'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  </a> </div>
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs smaller-page2">
                  <div class="pull-left datetime__berita____pilihan rilis__002__rilis smaller-page2">11/11/2017, 14.00 WIB</div>
                  <div class="clear"></div>
                  <div class="judul__berita_pilihan_02_rilis smaller-page2"><?php echo $rgm['judul_artikel']; ?></div>
                  <div class="tags__berita____pilihan rilis__002__rilis smaller-page2"><?php echo $rgm['nama_section']; ?></div>
                </div>
                <div class="clear"></div>
              </div>
              <?php }
                  $i++;
               } ?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 news__populer__pilihan__focus">
        <div class="widget-title st-2-4">
          <h3>KRAKATAU</h3>
          <div class="separator-6"></div>
        </div>
        <?php 
              $i=1; 
              foreach ($krakatau as $kkt){
                if ($i==1){
                ?>
        <div class="clear"></div>
        <div class="room__berita__pilihan">
          <div class="row berita__pilihan__rilis">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six__col-lg-6_rilis smaller-page2">
              <div class="pos-relative image__first__rilis__berita__pilihan smaller-page2"> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($kkt['postdate']));
                if(!empty($kkt['thumbnail'])){ 

                $mystring = $kkt['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                <div class="image______berita____pilihan rilis smaller-page2"><img data-original="<?php echo $tim.$upload.$path.$kkt['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                <?php }else{ ?>
                <div class="image______berita____pilihan rilis smaller-page2"><img data-original="<?php echo $tim.$kkt['thumbnail'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                <?php } ?>
                <?php }else{ ?>
                <div class="image______berita____pilihan rilis smaller-page2"><img data-original="<?php echo $tim.$kkt['oldimage'];?>&w=500&h=300&zc=0" alt="berita" class="lazy img100" style="display: inline;"></div>
                <?php } ?>
                </a> <a href="">
                <div class="tags___berita____pilihan rilis smaller-page2"><?php echo $kkt['nama_section']; ?></div>
                </a> </div>
              <div class="date__time___berita____pilihan rilis smaller-page2">11/11/2017, 17.00 WIB</div>
              <div class="judul____berita____pilihan rilis smaller-page2"><?php echo $kkt['judul_artikel']; ?></div>
              <div class="isi____berita____pilihan rilis smaller-page2"><?php echo $kkt['isi_artikel']; ?></div>
              </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six__col-lg-6_rilis smaller-page2">
              <?php }else { ?>
              <div class="kolom___berita____pilihan rilis__002__rilis smaller-page2">
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis smaller-page2"> <a href="">
                <?php 
                $path = date('Y/m/d/', strtotime($kkt['postdate']));
                if(!empty($kkt['thumbnail'])){ 

                $mystring = $kkt['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis smaller-page2"><img data-original="<?php echo $upload.$path.$kkt['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php }else { ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis smaller-page2"><img data-original="<?php echo $kkt['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis smaller-page2"><img data-original="<?php echo $kkt['oldimage'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;"></div>
                  <?php } ?>
                  </a> </div>
                <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs smaller-page2">
                  <div class="pull-left datetime__berita____pilihan rilis__002__rilis smaller-page2">11/11/2017, 14.00 WIB</div>
                  <div class="clear"></div>
                  <div class="judul__berita_pilihan_02_rilis smaller-page2"><?php echo $kkt['judul_artikel']; ?></div>
                  <div class="tags__berita____pilihan rilis__002__rilis smaller-page2"><?php echo $kkt['nama_section']; ?></div>
                </div>
                <div class="clear"></div>
              </div>
              <?php }
                  $i++;
               } ?>
            </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
          </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section id="video__gallery__rilis">
  <div class="cover__video__gallery__rilis">
    <div class="container">
      <div class="widget-title st-2-4">
        <h3 class="h3fff">Galeri Video</h3>
        <div class="separator-6"></div>
      </div>
      <div class="clear"></div>
      <div class="row gallery__video__rilis__page__home__small">
        
        <?php foreach ($video as $vd){
          if(!empty($vd['id_video'])){
            $ug = $tim.video_image($vd['id_video']);
          ?>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 gallery__video__rilis__page__home__small"> <a href="<?php echo site_url(url_title($vd['urltitle']));?>">
            <div class="post__gallery__video__small">
              <div class="image__video__small__rilis"><img src="<?php echo $ug;?>&w=750&h=500&z=1" alt="video rilis" title="video rilis" class="lazy img100 video__smaller"/></div>
              <div class="circle__play__rilis_small"><i class="fa fa-play playme__rilis_small"></i></div>
            </div>
            <div class="video__title__smaller__rilis"><span>VIDEO :</span> <?php echo $vd['judul_artikel'];?></div>
            </a> </div>
        <?php ;} } ?>
      </div>
    </div>
  </div>
</section>
<section id="kolom____artikel__rilis__dan__footer">
  <div class="container">
  <div class="row center__berita__pilihan__dan__footer">
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col_berita_pilihan_dan_populer_rilis">
      <div class="row section-bottom-lampung rilis">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six-section-bottom">
          <div class="widget-title st-2-4">
            <h3>SUARA KAMPUS</h3>
            <div class="separator-6"></div>
          </div>
          <div class="clear"></div>
          <div class="kolom-in-suarakampus-rilis">
            <?php
            foreach($suarakampus as $suarakampus) {?>
            <div class="kolom___berita____pilihan rilis__002__rilis in-pos-kategori suarakampus-rilis">
              <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis suarakampus-rilis"> 
                <a href="">
                  <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis suarakampus-rilis">

                     
                     <?php 
                        $path = date('Y/m/d/', strtotime($suarakampus['postdate']));
                        if(!empty($suarakampus['thumbnail'])){ 

                        $mystring = $suarakampus['thumbnail'];
                        $findme   = 'http';
                        $pos = strpos($mystring, $findme);
                          if ($pos === false) {?>

                             <img data-original="<?php echo $tim.$upload.$path.$suarakampus['thumbnail'];?>&w=500&h=262&zc=0" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                          <?php } else {?>

                             <img data-original="<?php echo $tim.$suarakampus['thumbnail'];?>&w=500&h=262&zc=0" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                          <?php }?>


                        <?php }else{ ?>


                               <img data-original="<?php echo $tim.$suarakampus['oldimage'];?>&w=500&h=262&zc=0" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                        <?php } ?>


                  </div>
                </a> 
              </div>
              <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs suarakampus-rilis">
                <div class="pull-left datetime__berita____pilihan rilis__002__rilis suarakampus-rilis">11/11/2017, 14.00 WIB</div>
                <div class="clear"></div>
                <div class="judul__berita_pilihan_02_rilis suarakampus-rilis"><?php echo $suarakampus['judul_artikel'];?></div>
                <a href="">
                <div class="tags__berita____pilihan rilis__002__rilis suarakampus-rilis">SUARA KAMPUS</div>
                </a> </div>
              <div class="clear"></div>
            </div>
            <?php ;} ?>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 six-section-bottom">
          <div class="widget-title st-2-4">
            <h3>INSPIRASI</h3>
            <div class="separator-6"></div>
          </div>
          <div class="clear"></div>
          <div class="in page-kolom-bottom-rilis">
            <?php
            foreach($inspirasi as $inspirasi) {?>
            <div class="page___kolom___rilis"> 
              <a href="<?php echo site_url($inspirasi['urltitle'].'.html');?>">
                <?php
                 if(!empty($inspirasi['image']))
                 {?>
                     <div class="pull-left img__kolom rilis"><img src="<?php echo $tim.$inspirasi['image'];?>" alt="kolom" title="kolom" width="300" height="300"/></div>

                 <?php } else { ?>
                 
                 <?php 
                    $path = date('Y/m/d/', strtotime($inspirasi['postdate']));
                    if(!empty($inspirasi['thumbnail'])){ 

                    $mystring = $inspirasi['thumbnail'];
                    $findme   = 'http';
                    $pos = strpos($mystring, $findme);
                      if ($pos === false) {?>

                         <div class="pull-left img__kolom rilis"><img data-original="<?php echo $tim.$upload.$path.$inspirasi['thumbnail'];?>" alt="kolom" title="kolom" width="300" height="300"/></div>

                      <?php } else {?>

                         <div class="pull-left img__kolom rilis"><img data-original="<?php echo $tim.$inspirasi['thumbnail'];?>" alt="kolom" title="kolom" width="300" height="300"/></div>

                      <?php }?>


                    <?php }else{ ?>

                           <div class="pull-left img__kolom rilis"><img data-original="<?php echo $tim.$inspirasi['oldimage'];?>" alt="kolom" title="kolom" width="300" height="300"/></div>

                    <?php } ?>

               <?php ;} ?>
              </a>
             <div class="pull-left isi___kolom rilis"> 
                <a href="<?php echo site_url(url_title($inspirasi['urltitle']).'.html');?>">
                <div class="title1__kolom__rilis"><?php echo $inspirasi['nama_section'];?></div>
                </a> 
                <a href="<?php echo site_url(url_title($inspirasi['urltitle']).'.html');?>">
                <div class="title2__kolom__rilis"><?php echo $inspirasi['judul_artikel'];?></div>
                </a>
                <?php
                if(!empty($inspirasi['nama'])) {
                  ?>
                  
                    <div class="name__kolom__rilis" style="cursor: pointer;"><?php echo $inspirasi['nama'];?></div>
                 
                <?php ;} else {?>
                <?php $adm = $this->T_admin->getid($inspirasi['id_admin']);?>
                  <a href="<?php echo site_url('penulis/'.$inspirasi['id_admin']);?>">
                    <div class="name__kolom__rilis"><?php echo $adm['nama'];?></div>
                  </a> 
                <?php ;} ?>
              </div>
              <div class="clear"></div>
            </div>
            <?php ;} ?>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col_berita_pilihan_dan_populer_rilis">
      <div class="widget-title st-2-4">
        <h3>OPINI</h3>
        <div class="separator-6"></div>
      </div>
      <div class="clear"></div>
      
      <div class="page-opini-rilis">
      <?php
        foreach($opini as $opini) {?>
        <div class="kolom___berita____pilihan rilis__002__rilis in-pos-kategori suaraopini-rilis">
        <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis suaraopini-rilis"> 
          <a href="">
            <div class="ovhidden ov__kolom___berita____pilihan rilis__002__rilis suaraopini-rilis">
             
                <?php
                 if(!empty($opini['image']))
                 {?>

                     <img data-original="<?php echo $tim.$opini['image'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                 <?php } else { ?>
                 
                 <?php 
                    $path = date('Y/m/d/', strtotime($opini['postdate']));
                    if(!empty($opini['thumbnail'])){ 

                    $mystring = $opini['thumbnail'];
                    $findme   = 'http';
                    $pos = strpos($mystring, $findme);
                      if ($pos === false) {?>

                         <img data-original="<?php echo $tim.$upload.$path.$opini['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                      <?php } else {?>


                         <img data-original="<?php echo $tim.$opini['thumbnail'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                      <?php }?>


                    <?php }else{ ?>

                           <img data-original="<?php echo $tim.$opini['oldimage'];?>" class="lazy img100" title="berita" alt="berita"  style="display: inline;">

                    <?php } ?>

                <?php ;} ?>
            </div>
          </a> 
        </div>
        <div class="pull-left img__kolom___berita____pilihan rilis__002__rilis rtexs suaraopini-rilis">
          <div class="pull-left datetime__berita____pilihan rilis__002__rilis suaraopini-rilis">11/11/2017, 14.00 WIB</div>
          <div class="clear"></div>
          <div class="judul__berita_pilihan_02_rilis suaraopini-rilis"><?php echo $opini['judul_artikel'];?></div>
          <a href="">
          <div class="tags__berita____pilihan rilis__002__rilis suaraopini-rilis">OPINI</div>
          </a> </div>
        <div class="clear"></div>
      </div>
      <?php ;} ?>
      </div>
    </div>
  </div>
</section>
<br/>
<div class="ads__top" style="background:url(<?php echo $images;?>zox-leader.png)"></div>
<br/>
<section id="kolom____artikel__rilis__dan__footer">
  <div class="container">
    <div class="row center__berita__pilihan__dan__footer">
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col_berita_pilihan_dan_populer_rilis">
        <div class="widget-title st-2-4">
          <h3>BERITA LAINNYA</h3>
          <div class="separator-6"></div>
        </div>
        <div class="clear"></div>
        <div class="banner-klan-middle-01"><img data-original="<?php echo $images;?>a1462a6f3eb901c258613a43cf3eb3fc.gif"/></div>
        <div class="row berita__lainnya">
          <?php foreach ($beritalainnya as $bl){ ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
               <?php 
                $path = date('Y/m/d/', strtotime($bl['postdate']));
                if(!empty($bl['thumbnail'])){ 

                $mystring = $bl['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100"data-original="<?php echo $upload.$path.$bl['thumbnail'];?>" alt="berita" title="berita" height="496" width="800"> </a> </div>
              <?php }else{ ?>
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100"data-original="<?php echo $bl['thumbnail'];?>" alt="berita" title="berita" height="496" width="800"> </a> </div>
              <?php } ?>
              <?php }else{ ?>
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100"data-original="<?php echo $bl['oldimage'];?>" alt="berita" title="berita" height="496" width="800"> </a> </div>
              <?php } ?>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#"><?php echo $bl['judul_artikel']; ?></a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya"><?php echo $bl['nama_section']; ?></div>
                  </a> </header>
              </div>
            </div>
          </div>
          <?php } ?>
          <div class="clear"></div>
          <div align="center"> <a href="more.html">
            <div class="center__more__berita__rilis">Lihat lebih banyak berita</div>
            </a> </div>
          <br/>
        </div>
      </div>
     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col_berita_pilihan_dan_populer_rilis" id="sidebarrightkategori">
        <div class="ads_rilis"><img src="<?php echo $images;?>rec300.jpg" title="ads" class="lazy img100" height="250" width="300"></div>
        <br/>
        <div class="widget-title st-2-4">
          <h3>Rilizen</h3>
          <div class="separator-6"></div>
        </div>
        <div class="clear"></div>
        <br/>
        <div class="kolom__point__rilis">
          <?php foreach($rilisen as $a){ ?>
          <div class="in__kolom__point__rilis"> <a href="">
            <div class="pull-left img__point__user"><img data-original="<?php echo $tim.$rilizen.$a['image'];?>" class="lazy img100" alt="poin rilis" title="poin rilis" src="<?php echo $tim.$rilizen.$a['image'];?>" style="display: inline;" height="50" width="50"></div>
            <div class="pull-left name__user__point__rilis">
              <div><?php echo $a['nama_lengkap'];?></div>
              <div class="poin__user__box"><?php echo $a['profesi'];?></div>
            </div>
            </a>
            <div class="clear"></div>
          </div>
          <?php } ?>

          <a href="detail-agenda.html">
          <div class="detail-agenda" style="padding-bottom:15px; padding-top:0px;">Lihat selengkapnya....</div>
          </a> </div>
        <div class="tag_footer_div"> 
          <?php foreach($menu as $a){ 
              $count =  count($a );?>
          <a href="<?php echo site_url('page/'.url_title($a['titleurl']));?>">
          <div class="left_t_footer_n01"><?php echo strtoupper($a['judul_hs']);?></div>
          </a> 
          <?php } ?>
          <div class="clear"></div>
        </div>
        <div align="center">
          <div class="apps__center">
            <div class="pull-left apps"><img src="<?php echo $images;?>google_play.png"></div>
            <div class="pull-left apps store"><img src="<?php echo $images;?>appstore.png"></div>
            <div class="clear"></div>
          </div>
        </div>
        <div align="center">
          <div class="social_media_footer"> <a href="http://<?php echo $sosmed['facebook'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-facebook fb"></i></div>
            </a> <a href="http://<?php echo $sosmed['twitter'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
            </a> <a href="http://<?php echo $sosmed['instagram'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
            </a> <a href="http://<?php echo $sosmed['gplus'];?>" target="_blank">
            <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
</body>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>
