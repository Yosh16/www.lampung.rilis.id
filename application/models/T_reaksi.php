<?php
/**
* 
*/
class T_reaksi extends CI_Model
{
	
	protected $_table = 't_reaksi';

	function getid($id)
	{
		$this->db->where('id_artikel', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function update($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data);
	}

	function add($data)
	{
		$this->db->insert($this->_table,$data);
	}
}