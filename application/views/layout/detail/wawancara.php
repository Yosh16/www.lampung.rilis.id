<div class="title-section" align="left">
          <h1><span class="world-purple">Wawancara</span></h1>
          <div class="red_title"><a href="<?php echo site_url('kategori/Wawancara');?>">Lainnya +</a></div>
          <div class="clear"></div>
        </div>
        <div class="line_box">
          <?php $wawancara = $this->T_artikel->getmenu(28,2,0);?>
          <?php 
            $path = date('Y/m/d/', strtotime($wawancara[0]['postdate']));
            if(!empty($wawancara[0]['thumbnail'])){ 

            $mystring = $wawancara[0]['thumbnail'];
            $findme   = 'http';
            $pos = strpos($mystring, $findme);
              if ($pos === false) {?>

                 <div class="img_fokus wawancara"><a href="<?php echo site_url($wawancara[0]['urltitle']).'.html';?>"> <img data-original="<?php echo $tim.$upload.$path.$wawancara[0]['thumbnail'];?>&w=531&h=402&zc=1" class="lazy img100 img-responsive" alt="fokus" height="302" width="531"> </a> </div>

              <?php } else {?>

                <div class="img_fokus wawancara"> <a href="<?php echo site_url($wawancara[0]['urltitle']).'.html';?>"><img data-original="<?php echo $tim.$wawancara[0]['thumbnail'];?>&w=531&h=402&zc=1" class="lazy img100 img-responsive" alt="fokus" height="302" width="531"></a></div>

              <?php }?>


            <?php }else{ ?>

                   <div class="img_fokus wawancara"> <a href="<?php echo site_url($wawancara[0]['urltitle']).'.html';?>"><img data-original="<?php echo $tim.$wawancara[0]['oldimage'];?>&w=531&h=402&zc=1" class="lazy img100 img-responsive" alt="fokus" height="302" width="531"></a></div>

            <?php } ?>
          <?php foreach($wawancara as $a){ ?>
          <div class="kolom_in">
            <div class="left_icon_focus"><i class=" fa fa-microphone oc1"></i></div>
            <div class="right_focus_berita wawancara"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>">
              <div class="paper_focus wawancara"><span>Wawancara:</span> <?php echo $a['judul_artikel'];?></div>
              </a>
              <div class="thumbail rilis__wawancara"><span class="focus_blue wawancara"><?php
                    $ex = explode(' ', $a['postdate']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('d/m/Y H.i', strtotime($a['postdate']));?></span></div>
            </div>
            <div class="clear"></div>
          </div>
          <?php } ?>
        </div>