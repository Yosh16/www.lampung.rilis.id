<?php 
class Page extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_reaksi');
		$this->load->model('T_komentar');
		$this->load->model('T_reaksi_log');
		$this->load->model('T_fokus');
		$this->load->model('T_halamanstatis');
		$this->load->model('T_informasi');
		$this->load->model('T_admin');
		$this->load->model('T_user');
		$this->load->model('T_section');
		$this->load->model('T_contact');
		date_default_timezone_set('Asia/Jakarta');
	}

	function index($id){
		$data['page'] = 'Page';
		$data['title'] = 'Rilis';
		//$detect = $this->T_artikel->get_detect($id);
		
		$data['detailhs']		=		$this->T_halamanstatis->get($id);
		
		
		//$related = $this ->T_artikel->get_artikel($id);
		//$data['recent'] = $this->T_artikel->recent(5,0);
		//$data['terkait'] = $this->T_artikel->related($related['id_section']);
		//$data['reaksi']	= $this->T_reaksi->getid($id);
		//$data['komentar'] = $this->T_komentar->getkomentar($id);
		//$data['komentarall'] = $this->T_komentar->getkomentarall($id);
		
			$this->load->view('page/index', $data);
		
	}

	function message(){
		$data = array(
				'nama_lengkap'	=>	$this->input->post('nama'),
				'email'			=>	$this->input->post('email'),
				'subject'		=>	$this->input->post('subject'),
				'pesan'			=>	$this->input->post('pesan'),
				'post_date'		=> date('Y-m-d H:i:s'));
			$this->T_contact->add($data);
			redirect('page/thanks');
		}	
	function thanks(){
		$data['page'] = 'Thanks';
		$data['title'] = 'Rilis';
			$this->load->view ('thanks/index', $data);
		
	}
}