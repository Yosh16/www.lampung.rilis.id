<?php
$base             = $this->config->item('base_url');
$css              = $this->config->item('css');
$images           = $this->config->item('images');
$font             = $this->config->item('font');
$js               = $this->config->item('js');
$fonts            = $this->config->item('fonts');
$tim              = $this->config->item('images_tim');
$upload           = $this->config->item('upload');
$rilizen           = $this->config->item('rilizen'); ?>

<?php foreach ($beritalainnya as $bl){ ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
               <?php 
                $path = date('Y/m/d/', strtotime($bl['postdate']));
                if(!empty($bl['thumbnail'])){ 

                $mystring = $bl['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($bl['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$upload.$path.$bil['thumbnail'];?>&w=800&h=496" alt="berita" title="berita" height="496" width="800"> </a> </div>
              <?php }else{ ?>
              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($bl['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$bl['thumbnail'];?>&w=800&h=496" alt="berita" title="berita" height="496" width="800"> </a> </div>
              <?php } ?>
              <?php }else{ ?>
              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($bl['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$bl['oldimage'];?>&w=800&h=496" alt="berita" title="berita" height="496" width="800"> </a> </div>
              <?php } ?>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date"><?php
                    $ex = explode(' ', $bl['postdate']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('d/m/Y H.i', strtotime($bl['postdate']));?> </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="<?php echo site_url(url_title($bl['urltitle']).'.html');?>"><?php echo substr($bl['judul_artikel'], 0, 35);?></a> </h4>
                  <a href="<?php echo site_url('kategori/'.url_title($bl['nama_section']))?>">
                  <div class="tag__berita__laiinya"><?php echo $bl['nama_section']; ?></div>
                  </a> </header>
              </div>
            </div>
          </div>
          <?php } ?>