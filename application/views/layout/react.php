<script type="text/javascript">

  function react(id, reaksi)
  {
    var user        = "<?php echo $this->session->userdata('id');?>";
    $.ajax({
        'type': 'POST',
        'url': '<?php echo site_url('detail/cekreaksi');?>',
        'dataType': 'json',
        'data': {'user' : user, 'id' : id},
        'success': function(data){
           if(data)
           {
              alert('Sorry, you have been reacted');
           }
           else
           {
              $.ajax({
                  'type': 'POST',
                  'url': '<?php echo site_url('detail/logreaksi');?>',
                  'dataType': 'json',
                  'data': {'id' : id, 'user' : user}
              });
              $.ajax({
                  'type': 'POST',
                  'url': '<?php echo site_url('detail/reaksi');?>',
                  'dataType': 'json',
                  'data': {'id' : id, 'reaksi' : reaksi},
                  'success': function(data){
                      //console.log(data);
                      var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                      //console.log(jumlah);
                      if(reaksi == 'suka')
                      {
                        var result = parseInt(data.suka) / parseInt(jumlah) * 100;
                      }
                      else if(reaksi == 'sedih')
                      {
                        var result = parseInt(data.sedih) / parseInt(jumlah) * 100;
                      }
                      else if(reaksi == 'tidak_suka')
                      {
                        var result = parseInt(data.tidak_suka) / parseInt(jumlah) * 100;
                      }
                      else if(reaksi == 'terkejut')
                      {
                        var result = parseInt(data.terkejut) / parseInt(jumlah) * 100;
                      }
                      else if(reaksi == 'bosen')
                      {
                        var result = parseInt(data.bosen) / parseInt(jumlah) * 100;
                      }
                      else if(reaksi == 'bingung')
                      {
                        var result = parseInt(data.bingung) / parseInt(jumlah) * 100;
                      }
                      else if(reaksi == 'marah')
                      {
                        var result = parseInt(data.marah) / parseInt(jumlah) * 100;
                      }
                      
                      var string = result.toString();
                      var final = string.substr(0, 4)
                      $("#"+reaksi+"result").text(final+'%');
                      console.log(final+'%');
                  }
              });
           }
        }
    });
  }


  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#suka").data("id");
        var reaksi      = $("#suka").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.suka) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#sukaresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })

  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#sedih").data("id");
        var reaksi      = $("#sedih").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.sedih) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#sedihresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })

  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#tidak_suka").data("id");
        var reaksi      = $("#tidak_suka").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.tidak_suka) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#tidak_sukaresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })

  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#terkejut").data("id");
        var reaksi      = $("#terkejut").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.terkejut) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#terkejutresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })

  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#bosen").data("id");
        var reaksi      = $("#bosen").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.bosen) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#bosenresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })

  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#bingung").data("id");
        var reaksi      = $("#bingung").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.bingung) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#bingungresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })

  $(document).ready(function() {
    $(window).load(function() {
        var id          = $("#marah").data("id");
        var reaksi      = $("#marah").data("reaksi");
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('detail/getreaksi');?>',
            'dataType': 'json',
            'data': {'id' : id, 'reaksi' : reaksi},
            'success': function(data){
                var jumlah = parseInt(data.suka) + parseInt(data.sedih) + parseInt(data.tidak_suka) + parseInt(data.terkejut) + parseInt(data.bosen) + parseInt(data.bingung) + parseInt(data.marah);
                var result = parseInt(data.marah) / parseInt(jumlah) * 100;
                var string = result.toString();
                var final = string.substr(0, 4)
                $("#marahresult").text(final+'%');
                console.log(final+'%');
            }
        });
    })
  })
</script>
