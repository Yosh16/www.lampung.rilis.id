<?php
   $base             = $this->config->item('base_url');
   $css              = $this->config->item('css');
   $images           = $this->config->item('images');
   $font             = $this->config->item('font');
   $js               = $this->config->item('js');
   $fonts            = $this->config->item('fonts');
   $tim              = $this->config->item('images_tim');
   $upload           = $this->config->item('upload');
   $rilizen          = $this->config->item('rilizen');
   $mimin            = $this->config->item('mimin');
   
   function video_image($url){
   $image_url = parse_url($url);
   if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
   $array = explode("&", $image_url['query']);
   return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
   } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
   $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
   return $hash[0]["thumbnail_small"];
   }
   } 
   
   function video_id($url){
   $image_url = parse_url($url);
   if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
   $array = explode("&", $image_url['query']);
   return substr($array[0], 2);
   } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
   $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
   return $hash[0]["thumbnail_small"];
   }
   }
   ?>
<script src="<?php echo $js;?>jquery.min.js"></script>
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script>
   var OneSignal = window.OneSignal || [];
     OneSignal.push(function() {
       OneSignal.init({
         appId: "e4ea1e4b-2c60-4401-b958-db83b5573694",
       });
     });
</script>
<script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
   
     ga('create', 'UA-77188629-1', 'auto');
     ga('send', 'pageview');
</script>
<script type='text/javascript'>
   var googletag = googletag || {};
           googletag.cmd = googletag.cmd || [];
           (function() {
             var gads = document.createElement('script');
             gads.async = true;
             gads.type = 'text/javascript';
             var useSSL = 'https:' == document.location.protocol;
             gads.src = (useSSL ? 'https:' : 'http:') +
               '//www.googletagservices.com/tag/js/gpt.js';
             var node = document.getElementsByTagName('script')[0];
             node.parentNode.insertBefore(gads, node);
           })();
</script>
<script>
   googletag.cmd.push(function() {
       googletag.defineSlot('/65187598/728', [728, 90], 'div-gpt-ad-1487500530922-0').addService(googletag.pubads());
       googletag.pubads().enableSingleRequest();
       googletag.enableServices();
     });
</script>
<script>
   googletag.cmd.push(function() {
       googletag.defineSlot('/65187598/400', [468, 60], 'div-gpt-ad-1488741283122-0').addService(googletag.pubads());
       googletag.pubads().enableSingleRequest();
       googletag.enableServices();
     });
</script>
<script type='text/javascript'>
   googletag.cmd.push(function() {
             googletag.defineSlot('/65187598/1001', [300, 250], 'div-gpt-ad-1461870508354-0').addService(googletag.pubads());
             googletag.pubads().enableSingleRequest();
             googletag.enableServices();
           });
</script>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
   _atrk_opts = { atrk_acct:"GFJOo1IWhe10WR", domain:"rilis.id",dynamic: true};
   (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript>
   <img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=GFJOo1IWhe10WR" style="display:none" height="1" width="1" alt="" />
</noscript>

  <?php 
  if(!empty($detail)){
  $path = date('Y/m/d/', strtotime($detail['postdate']));
  if(!empty($detail['thumbnail_watermark'])){?>
    <?php $imageurl = $detail['thumbnail_watermark'];?>
  <?php } elseif(!empty($detail['thumbnail'])){
    $mystring = $detail['thumbnail'];
    $findme   = 'http';
    $pos = strpos($mystring, $findme);
      if ($pos === false) {?>
      <?php $imageurl = $upload.$path.$detail['thumbnail'];?>
        <?php }else{ ?>
      <?php $imageurl = $detail['thumbnail'];?>
        <?php }?>
      <?php ;} else {?>
      <?php $imageurl = $detail['oldimage'];?>
  <?php ;}?>
  <?php } else {'';} ?>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <?php if($page == "Detail") { ?>
<title><?php echo strip_tags($detail['judul_artikel']); ?> - RILIS.ID</title>
<?php }else{ ?>
<title>RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam</title>
<?php } ?>

<?php if($page == "Detail") { ?>
  <meta name="title" content="<?php echo strip_tags($detail['judul_artikel']); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php if(!empty($detail['meta_des'])){
  echo strip_tags(substr($detail['meta_des'],0,150)); } else { echo strip_tags(substr($detail['isi_artikel'], 0,150)); } ?>" />
  <meta name="keywords" content="<?php echo strip_tags(substr($detail['meta_key'],0,150));?>" />
  <meta name="alt_image" content="<?php echo strip_tags($detail['oldimage']);?>">
  <meta name="title_image" content="<?php echo strip_tags(substr($detail['judul_artikel'],0,150)); ?>">
  <meta name="author" content="RILIS.ID" />

  <meta property="fb:pages" content="" />
  <meta property="fb:app_id" content="" />
  <meta property="og:locale" content="id_ID" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo strip_tags($detail['judul_artikel']); ?>"/>
  <meta property="og:image" itemprop="image" content="<?php echo $imageurl;?>"/>
  <meta property="og:description" content="<?php if(!empty($detail['meta_des'])){ echo strip_tags(substr($detail['meta_des'],0,150)); }else{ echo strip_tags(substr($detail['isi_artikel'],0,150)); } ?>" />
  <meta property="og:url" content="<?php echo current_url();?>" />
  <meta property="og:site_name" content="RILIS.ID"/>
  <meta property="og:image:type" content="image/jpg" />
  <meta property="og:image:width" content="600" />
  <meta property="og:image:height" content="315" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@rilisonline">
  <meta name="twitter:title" content="<?php echo strip_tags($detail['judul_artikel']); ?>" />
  <meta name="twitter:image" content="<?php echo $imageurl;?>" />
  <meta name="twitter:url" content="<?php echo current_url();?>" />
  <meta name="twitter:description" content="<?php if(!empty($detail['meta_des'])) { echo strip_tags(substr($detail['meta_des'],0,150)); }else{ echo strip_tags(substr($detail['isi_artikel'], 0,150)); } ?>" />
<?php }elseif ($page == "Page") { ?>
  <meta name="title" content="<?php echo strip_tags($detail['judul_hs']); ?>-RILIS.ID Situs Berita Politik Indonesia ">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo strip_tags(substr($detail['konten_hs'], 0, 150)); ?>" />
  <meta name="keywords" content="<?php echo strip_tags($detail['judul_hs']);?>-RILIS.ID Situs Berita Politik Indonesia" />
  <meta name="alt_image" content="<?php echo $images;?>sticky-logo-r.png">
  <meta name="title_image" content="<?php echo strip_tags($detail['judul_artikel']);?>">
  <meta name="author" content="RILIS.ID" />

  <meta property="fb:pages" content="" />
  <meta property="fb:app_id" content="" />
  <meta property="og:locale" content="id_ID" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo strip_tags($detail['judul_hs']); ?>-RILIS.ID Situs Berita Politik Indonesia"/>
  <meta property="og:image" itemprop="image" content="<?php echo $images;?>sticky-logo-r.png"/>
  <meta property="og:description" content="<?php echo strip_tags(substr($detail['konten_hs'], 0, 150)); ?>" />
  <meta property="og:url" content="<?php echo current_url();?>" />
  <meta property="og:site_name" content="RILIS.ID"/>
  <meta property="og:image:type" content="image/jpg" />
  <meta property="og:image:width" content="600" />
  <meta property="og:image:height" content="315" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@rilisonline">
  <meta name="twitter:title" content="<?php echo strip_tags($detail['judul_hs']); ?>-RILIS.ID Situs Berita Politik Indonesia" />
  <meta name="twitter:image" content="<?php echo $images;?>sticky-logo-r.png" />
  <meta name="twitter:url" content="<?php echo current_url();?>" />
  <meta name="twitter:description" content="<?php echo strip_tags(substr($detail['konten_hs'], 0, 150)); ?>" />

<?php }else{ ?>
  <meta name="title" content="<?php echo strip_tags($page); ?>-RILIS.ID Situs Berita Politik Indonesia">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo strip_tags($page); ?>-RILIS.ID Situs Berita Politik Indonesia" />
  <meta name="keywords" content="<?php echo strip_tags($page);?>-RILIS.ID Situs Berita Politik Indonesia" />
  <meta name="alt_image" content="<?php echo $images;?>sticky-logo-r.png">
  <meta name="title_image" content="<?php echo strip_tags($page);?>-RILIS.ID Situs Berita Politik Indonesia">
  <meta name="author" content="RILIS.ID" />

  <meta property="fb:pages" content="" />
  <meta property="fb:app_id" content="" />
  <meta property="og:locale" content="id_ID" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo strip_tags($page); ?>RILIS.ID Situs Berita Politik Indonesia"/>
  <meta property="og:image" itemprop="image" content="<?php echo $images;?>sticky-logo-r.png"/>
  <meta property="og:description" content="<?php echo strip_tags($page); ?>RILIS.ID Situs Berita Politik Indonesia" />
  <meta property="og:url" content="<?php echo current_url();?>" />
  <meta property="og:site_name" content="RILIS.ID"/>
  <meta property="og:image:type" content="image/jpg" />
  <meta property="og:image:width" content="600" />
  <meta property="og:image:height" content="315" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@rilisonline">
  <meta name="twitter:title" content="<?php echo strip_tags($page); ?>RILIS.ID Situs Berita Politik Indonesia" />
  <meta name="twitter:image" content="<?php echo $images;?>sticky-logo-r.png" />
  <meta name="twitter:url" content="<?php echo current_url();?>" />
  <meta name="twitter:description" content="<?php echo strip_tags($page); ?>RILIS.ID Situs Berita Politik Indonesia" />
<?php } ?>
      <link rel="shortcut icon" type="image/png" href="<?php echo $images; ?>favicon.png"/>
      <link rel="shortcut icon" type="image/png" href="<?php echo $images; ?>favicon.png"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $fonts;?>font-awesome.min.css"/>
      <link rel="stylesheet" href="<?php echo $css ;?>bootstrap.min.css"/>
      <link rel="stylesheet" href="<?php echo $css ;?>owl.carousel.css"/>
      <link rel="stylesheet" href="<?php echo $css ;?>animate.css"/>
      <link rel="stylesheet" href="<?php echo $css ;?>rilis.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $css;?>fancybox.min.css"/>
      <?php if($page == "Kategori_Agenda") {?>
      <link rel="stylesheet" href="<?php echo $css ;?>table-lain.css"/>
      <?php } else { ?>
      <link rel="stylesheet" href="<?php echo $css;?>table-home.css"/>
      <link rel="stylesheet" href="<?php echo $css;?>animate.css"/>
      <?php } ?>
   </head>
   <body>
      <div class="modal fade" id="searching-article" role="dialog">
         <div class="modal-dialog searching-article">
            <div class="modal-content searching-article">
               <div class="modal-header searching-article">
                  <button type="button" class="close searching-article" data-dismiss="modal">&times;</button>
               </div>
               <form action="<?php echo site_url('search/index');?>" method="get">
                  <div class="modal-body searching-article">
                     <div class="searching-article-input">
                        <input name="search" type="text" placeholder="Cari artikel Berita" required/>
                        <i class="fa fa-search searching-article-fa"></i> 
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="ads__top2" style="background:url(<?php echo $images;?>bnk.jpg); "></div>
      <div class="header-sticky">
         <div class="container">
            <div class="header-menu-rilis">
               <div class="pull-left menu-sticky-header">
                  <a href="<?php echo site_url('home');?>">
                     <div class="pull-left icon-fa-home"><i class="fa fa-home"></i></div>
                  </a>
                  <a href="kategori.html">
                     <div class="pull-left sub-menu-sticy">
                        <?php
                           $i = 1;
                           $section = $this->T_section->getsection();
                           foreach($section as $section) {
                             if($section['nama_section'] == 'Kolom')
                             {
                                 $class = '';
                             }
                             
                             $child = $this->T_section->getsupsection($section['id_section']);
                             $chold = $this->T_section->getsupsection($section['id_section']); ?>
                        <?php if(!empty($child)) {?>
                     </div>
                  </a>
                  <div class="dropdown nav_menu pull-left sticky">
                     <a class="dropdown-toggle post_toogle_color" type="button" data-toggle="dropdown" data-hover="dropdown" data-animations="fadeInUp"><?php echo strtoupper($section['nama_section']);?> <span class="caret caretan"></span> </a>
                     <ul class="dropdown-menu post_toogle_color dropdownhover-bottom">
                        <div class="room_dropdonwn_01">
                           <div class="room_dropdonwn_00">
                              <?php 
                                 $i = 0;
                                 foreach($child as $child) { ?>
                              <a href="<?php echo site_url('kategori/'.$child['url_title']);?>">
                                 <div class="room_dropdonwn_00_title_1"><?php echo $child['nama_section'];?></div>
                              </a>
                              <?php $i++ ;} ?>
                           </div>
                           <div class="clear"></div>
                        </div>
                     </ul>
                     <?php }else{ ?>
                     <div class="dropdown nav_menu pull-left"> <a href="<?php echo site_url('kategori/'.$section['url_title']);?>" class="dropdown-toggle post_toogle_color" type="button"  data-animations="fadeInUp"><?php echo strtoupper($section['nama_section']);?> </a> </div>
                     <?php }} ?>
                  </div>
                  <!--<a href="<?php //echo site_url('kategori/agenda'); ?>">
                     <div class="pull-left sub-menu-sticy">AGENDA</div>
                     </a>--> 
                  <a href="<?php echo site_url('indeks');?>">
                     <div class="pull-left icon-fa-home last"><i class="fa fa-list-alt"></i></div>
                  </a>
               </div>
               <!-- <a href="">
                  <div class="pull-left pencil-rilizen-sticky"><i class="fa fa-pencil-square-o pencilsquare"></i></div>
                  </a> <a href="">
                  <div class="pull-left message-header-sticky"><i class="fa fa-envelope-o message-sticy"></i></div>
                  </a> <a href="">
                  <div class="pull-left bell-sticky-header"><i class="fa fa-bell-o bell-o-sticy"></i></div>
                  </a> -->
               <div class="pull-left search-sticky-header" data-toggle="modal"  data-target="#searching-article"><i class="fa fa-search search-sticky"></i></div>
               <div class="pull-left sticky-login-daftar-rilis sticky">
                  <div class="kolom__login__dan__daftar log sticky"><a href="http://rilis.id/daftar">Daftar</a><span>/</span><a href="http://rilis.id/login">Masuk</a></div>
               </div>
               <div class="clear"></div>
            </div>
            <div class="clear"></div>
         </div>
         <div class="clear"></div>
      </div>
      <header>
         <div class="header_first_rilis">
            <div class="container pos-relative">
               <div class="header_fixed">
                  <div class="logo__first pull-left"><a href="<?php echo site_url('home');?>"><img src="<?php echo $images;?>rilis-lampung-nohoax.png" class="img__logo__first__rilis" alt="logo rilis" title="logo rilis" height="266" width="730"></a></div>
                  <div class="pull-left center__header__rilis">
                     <form action="<?php echo site_url('search/index');?>" method="get">
                        <div class="search_first_header">
                           <input name="search" placeholder="Search..." class="input_text_0_headline" type="text">
                           <div class="search_me_0"><i class="fa fa-search search_ddd" aria-hidden="true"></i></div>
                        </div>
                     </form>
                     <a href="http://dev.rilis.id/www.rilis.id/rilizen">
                        <div class="kolom__login__dan__daftar cusomcj pull-left"> Rilizen</div>
                     </a>
                     <div class="pull-right right__header__rilis">
                        <div class="kolom__login__dan__daftar log"><a href="http://rilis.id/daftar">Daftar</a><span>/</span><a href="http://rilis.id/login">Masuk</a></div>
                        <div class="kolom__two__right_rilis"><span><i class="fa fa-envelope letter-message-rilis"></i></span> <span><i class="fa fa-bell-o notif-rilis"></i></span></div>
                     </div>
                     <div class="clear"></div>
                     <div align="center">
                        <div class="nav_header_in">
                           <a href="<?php echo site_url('home');?>">
                              <div class="nav_header_in_01 dropdown icon active"><i class="fa fa-home home-rilis-lampung"></i></div>
                           </a>
                           <div class="nav_header_in_01 dropdown">
                              <?php
                                 $i = 1;
                                 $section2 = $this->T_section->getsection();
                                 foreach($section2 as $section2) {
                                   
                                   $child2 = $this->T_section->getsupsection($section2['id_section']);
                                   $chold2 = $this->T_section->getsupsection($section2['id_section']);
                                   if(!empty($child2)) {?>
                                   <div class="dropdown nav_menu pull-left"> <a class="dropdown-toggle post_toogle_color" type="button" data-toggle="dropdown" data-hover="dropdown" data-animations="fadeInUp"><?php echo strtoupper($section2['nama_section']);?> <span class="caret caretan"></span> </a>
                                    <ul class="dropdown-menu post_toogle_color dropdownhover-bottom">
                                      <div class="room_dropdonwn_01">
                                        <div class="room_dropdonwn_00"> 
                                        <?php foreach($child2 as $child2) {?>
                                          <a href="<?php echo site_url('kategori/'.$child2['url_title']);?>">
                                            <div class="room_dropdonwn_00_title_1"><?php echo $child2['nama_section'];?></div>
                                          </a>
                                        <?php ;} ?>
                                          </div>
                                        <div class="clear"></div>
                                      </div>
                                    </ul>
                                  </div>
                              <?php }else{ ?>
                              <div class="dropdown nav_menu pull-left"> <a href="<?php echo site_url('kategori/'.$section2['url_title']);?>" class="dropdown-toggle post_toogle_color" type="button"  data-animations="fadeInUp"><?php echo strtoupper($section2['nama_section']);?>  </a>
                              </div>
                              <?php }} ?>
                           </div>
                           <!-- div class="dropdown nav_menu pull-left"> <a class="dropdown-toggle post_toogle_color" type="button" data-toggle="dropdown" data-hover="dropdown" data-animations="fadeInUp">KOLOM <span class="caret caretan"></span> </a>
                              <ul class="dropdown-menu post_toogle_color dropdownhover-bottom">
                               <div class="room_dropdonwn_01">
                               <div class="room_dropdonwn_00"> <a href="">
                                <div class="room_dropdonwn_00_title_1">- Suara Kampus</div>
                                </a> <a href="">
                                <div class="room_dropdonwn_00_title_1">- Inspirasi</div>
                                </a> <a href="">
                                <div class="room_dropdonwn_00_title_1">- Opini</div>
                                </a> </div>
                              <div class="clear"></div>
                                </div>
                              </ul> -->
                        </div>
                        <a href="<?php echo site_url('indeks');?>">
                           <div class="icon_001"><i class="fa fa-list-alt" aria-hidden="true"></i></div>
                        </a>
                        <div class="clear"></div>
                     </div>
                  </div>
               </div>
               <div class="clear"></div>
            </div>
            <!--header fixed--> 
         </div>
         </div>
      </header>
      <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5a77ba3d10fe560012c5e92f&product=inline-share-buttons' async='async'></script>
      <?php
         function ago($time)
         {
           $time = strtotime($time);
            $periods = array("detik", "menit", "jam", "hari", "minggu", "bulan", "tahun", "dekade");
            $lengths = array("60","60","24","7","4.35","12","10");
         
            $now = time();
         
                $difference     = $now - $time;
                $tense         = "lalu";
         
            for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
                $difference /= $lengths[$j];
            }
         
            $difference = round($difference);
         
            if($difference != 1) {
                $periods[$j].= "";
            }
         
            return "$difference $periods[$j] 'lalu' ";
         }
         ?>