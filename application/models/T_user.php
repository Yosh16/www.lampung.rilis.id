<?php
class T_user extends CI_Model
{
	protected $_table = 't_user';

	function getuser($limit=7)
	{
		$this->db->limit($limit);
		$this->db->join('t_user_detail', 't_user_detail.user_id = t_user.id');
		return $this->db->get($this->_table)->result_array();
	}

	function getid($id)
	{
		$this->db->join('t_user_detail', 't_user.id = t_user_detail.user_id');
		$this->db->where('t_user.id', $id);
		return $this->db->get($this->_table)->row_array();
	}

}