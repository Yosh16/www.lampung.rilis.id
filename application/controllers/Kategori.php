<?php 
class Kategori extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_agenda');
		$this->load->model('T_section');
		$this->load->model('T_user');
		$this->load->model('T_halamanstatis');
		$this->load->model('T_informasi');
		$this->load->model('T_agenda');
		$data['title'] = 'Rilis';
		date_default_timezone_set('Asia/Jakarta');
	}

	function index($id)
	{
		$data['page'] = 'Kategori';
		$data['title'] = 'Rilis';
		$a = $this->T_artikel->kategori2($id);
		$data['page'] 	  = 'Kategori';

		if (isset($_GET['date_agenda'])){
			$date_agenda=date_create($_GET['date_agenda']);
			$date_agenda=date_format($date_agenda,"Y-m-d");
		}else{
			$date_agenda = 	date('Y-m-d');
			$date_agenda= date("Y-m-d", strtotime($date_agenda));
		}
		//$data['headline'] = $this->T_artikel->getmenu($id,1,0);
		//$data['menu'] 	  = $this->T_artikel->getmenu($id,4,1);
		//$data['lainnya']  = $this->T_artikel->getmenu($id,6,5);
		$data['test'] = $this->T_artikel->kategori2($id);
		$data['headline'] 			= $this->T_artikel->getmenu($a['id_section'],1,0);
		$data['menu'] 	  			= $this->T_artikel->getmenu($a['id_section'],4,1);
		$data['menus']   			= $this->T_halamanstatis->select();
		$data['foto_populer'] 		= $this->T_artikel->get_fotopopuler(139);
		$data['foto_lain'] 			= $this->T_artikel->get_fotolain(139);
		$data['rilisen'] 			= $this->T_artikel->sort();
		$data['lainnya']  			= $this->T_artikel->getmenu($a['id_section'],3,1);
		// $data['populer']  = $this->T_artikel->populer();
		$data['user']  	  			= $this->T_artikel->sort();
		$data['static']   			= $this->T_halamanstatis->select();
		$data['sosmed']   			= $this->T_informasi->get();
		$data['headline_foto'] 	  	= $this->T_artikel->getfoto(1,0);
		$data['foto'] 	  			= $this->T_artikel->getfoto(2,1);
		$data['foto2'] 	  			= $this->T_artikel->getfoto(3,3);
		$data['polemik']  			= $this->T_artikel->getmenu(131,10,0);
		$data['headline_vid'] 	  	= $this->T_artikel->getmenu(141,1,0);
		$data['agenda'] 			= $this->T_artikel->agenda(5,$date_agenda);
		$data['terkini'] 	  		= $this->T_artikel->getfoto(4,0);
		$data['terpopuler']   		= $this->T_artikel->hot_foto(4,0);
		$data['terkini_essay'] 	  	= $this->T_artikel->getessay(12,0);
		$data['terpopuler_essay']   = $this->T_artikel->hot_essay(4,0);
		$data['populer']   	  		= $this->T_artikel->hot_video(4);
		$data['populerberita'] 		= $this->T_artikel->populer();
		$data['terkini_vid'] 	  	= $this->T_artikel->getmenu(141,4,0);
		$data['infografis'] 	  	= $this->T_artikel->getmenu(127);
		$data['date_agenda']		= $date_agenda;
		//print_r($a);
		//die();
		
		if($a['id_section'] == 140)
		{
			$this->load->view('kategori/foto', $data);
		} 
		elseif($a['id_section'] == 141)
		{
			$this->load->view('kategori/video', $data);
		}
		elseif($a['id_section'] == 147)
		{
			$this->load->view('kategori/agenda', $data);
		}
		else
		{
			$this->load->view('kategori/index', $data);
		}
	}

	function more($j, $cat)
	{
		$a = $this->T_artikel->kategori2($cat);
		$data['lainnya']  = $this->T_artikel->getmenumore($a['id_section'],3,$j);
		$this->load->view('kategori/more', $data);
	}
	function morevideo($j, $cat)
	{
		$a = $this->T_artikel->kategori2($cat);
		$data['terkini_vid'] 	  = $this->T_artikel->getmenumore(141, 4, $j);
		$this->load->view('kategori/morevideo', $data);
	}

	function morefoto($j, $cat)
	{
		$a = $this->T_artikel->kategori2($cat);
		$data['terkini'] 	  = $this->T_artikel->getfotomore(4,$j);
		$this->load->view('kategori/morefoto', $data);
	}

	function agenda()
	{
		$data['page'] = 'Kategori_Agenda';
		$data['title'] = 'Rilis';
		$data['user']  	  = $this->T_artikel->sort();
		$data['populer']  = $this->T_artikel->populer();
		$data['rilisen'] = $this->T_artikel->sort();
		print_r($_GET['date_agenda']);
		exit();
		die();
		if (isset($_GET['date_agenda'])){
			$date_agenda = 	date('Y-m-d', strtotime($_GET['date_agenda']));
		}else{
			$date_agenda = 	date('Y-m-d');
		}
		$data['agenda'] = $this->T_agenda->getdata();
		$this->load->view('kategori/agenda', $data);
	}
	function komentar()
	{
		$this->load->view('kategori/komentar');		
	}

}