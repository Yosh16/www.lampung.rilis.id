<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#sort").change(function() {
      var filter = $(this).val();
      $("#filter").val(filter);
      $("#btnstatus").click();
    });
  })
</script>
<script>
 $(document).ready(function(){
     i=0;
     $('#more').click(function(){
       var data = $('.form-user3').serialize();
        i = i+12;
        // $('#result').html('');
        j = i;

        var id = $('#filter').val();
        
       console.log(data);
        $('#more_text').val(j);
         $.ajax({
            url:'<?php echo site_url('indeks/more/');?>'+j+'/'+id,
            method:'get',
             data:data,
             dataType:'text',
             success:function(data)
             {
               $('#result').append(data);
             }
         });
      
     });
   });
 </script>
 <!-- <script> 
 $(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
 });
 $(window).load(function(){
        $("#panel").slideUp("fast");
    });
 </script> -->
<section id="kategori-page-rilis">
    <div class="container">
        <div class="row col-lg-9-3-detail-page">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 detail-page-rilis">
                <div class="room_center_slider_headline">
                    <!--content-->
                    <div class="sub_menu_page_detail">
                        <div class="nama_sub0001"><a href="index.html">HOME</a></div>
                        <div class="right_sub"><i class="fa fa-angle-double-right angle_0111"></i></div>
                        <div class="nama_sub0001"><a href="kategori-foto.html">INDEKS</a></div>

                        <div class="clear"></div>
                    </div>


                    <div class="room-kategori-and-date">
                        <div class="pull-left select_0 select_komentar custom__indeks">
                             <select class="selecoby indeks" id="sort">
                                <option selected="">Filter Kategori Berita</option>
                                <?php
                                $i = 1;
                                $section = $this->T_section->getsectionindeks();
                                foreach($section as $section) {
                                  if($section['nama_section'] == 'Peristiwa')
                                  {
                                      $class = '';
                                  }
                                  elseif($section['nama_section'] == 'Ragam')
                                  { 
                                      $class = 'ragam';
                                  }
                                  elseif($section['nama_section'] == 'Inspirasi')
                                  {
                                      $class = 'inspirasi';
                                  }
                                  elseif($section['nama_section'] == 'Potret')
                                  {
                                      $class = 'potret';
                                  }
                                  $child = $this->T_section->getsupsection($section['id_section']);
                                  $chold = $this->T_section->getsupsection($section['id_section']);
                                  if(!empty($child)) {?>
                                <optgroup label="<?php echo strtoupper($section['nama_section']);?>">
                                <?php 
                                        foreach($child as $child) { ?>
                                        <option value="<?php echo $child['id_section'];?>"><?php echo $child['nama_section'];?></option>
                                        <?php ;} ?>
                                        </optgroup>
                                <?php }else{ ?>
                                <option value="<?php echo $section['id_section'];?>"><?php echo $section['nama_section'];?></option>
                                <?php } };?>
                               <!--  <form action="kategori" method="POST">
                              <input type="text" name="coba" id="coba">
                              <input type="submit" id="coba" style="display: none;">
                                </form> -->
                                <form action="<?php echo site_url('home/allartikel')?>" method="POST">
                              <input type="hidden" name="test" id="test">
                              <input type="submit" id="status" style="display: none;">
                              </form>

                                <form action="<?php echo site_url('indeks')?>" method="POST">
                              <input type="hidden" name="filter" id="filter" value="<?php echo $filter;?>">
                              <input type="submit" id="btnstatus" style="display: none;">
                                </form>
                              </select>
                        </div>
                        <form action="" class="form-horizontal" role="form">
                            <div class="pull-right tab_01_date_time">
                                <div class="input-group date form_datetime col-md-12 tab_03_date_time" data-date="2018-01-11" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
                                    <input class="form-control tab_03_date_time" size="16" type="text" value="01 Jan 2018" readonly>
                                    <span class="input-group-addon tab_03_date_time"><span class="glyphicon glyphicon-remove tab_03_date_time"></span></span><span class="input-group-addon tab_03_date_time"><span class="glyphicon glyphicon-calendar tab_03_date_time"></span></span>
                                </div>
                                <input type="hidden" id="dtp_input1" value="" />
                            </div>
                        </form>
                        <div class="clear"></div>
                    </div>
                    <form class="form-user3">
                        <input type="hidden" name="more_text" id="more_text" value="12">
                    </form> 
                    <div class="row berita__lainnya" id="result">
                       <?php foreach($all as $a){?>
          <?php if($a['publish'] == 'Y'){ ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">

          <?php if($a['id_section'] == 141) {?>
            <?php if(!empty($a['id_video'])){
              $urlg = $tim.video_image($a['id_video']); ?>

              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $urlg;?>&w=800&h=496&cz=1" alt="berita" title="berita" height="496" width="800"> </a> </div><div class="circle__play__rilis" style="position: absolute; left: 39%; top: 20%;"><i class="fa fa-play playme__rilis"></i></div>

            <?php } ?>
            <?php }else{ ?>

            <!-- MULAI GAMBAR -->            
            <?php  $path = date('Y/m/d/', strtotime($a['postdate']));
              if(!empty($a['thumbnail'])) {
                $mystring = $a['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$upload.$path.$a['thumbnail'];?>&w=800&h=496&zc=1" alt="berita" title="berita" height="496" width="800"> </a> </div>
            <?php }else{ ?>
              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$a['thumbnail'];?>&w=800&h=496&zc=1" alt="berita" title="berita" height="496" width="800"> </a> </div>
            <?php } ?>
            <?php ;} else {?>
              <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html');?>"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$a['oldimage'];?>&w=800&h=496&zc=1" alt="berita" title="berita" height="496" width="800"> </a> </div>
            <?php } ?>
            <!-- MULAI GAMBAR -->
            <?php } ?>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date"><?php
                    $ex = explode(' ', $a['tgl_pub']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('d/m/Y H.i', strtotime($a['tgl_pub']));?> </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="<?php echo site_url(url_title($a['urltitle']).'.html');?>"><?php echo substr($a['judul_artikel'], 0, 30);?></a> </h4>
                  <a href="<?php echo site_url('kategori/'.url_title($a['nama_section']))?>">
                  <div class="tag__berita__laiinya"><?php echo $a['nama_section'];?></div>
                  </a> </header>
              </div>
            </div>
          </div>
          <?php } else {'';} ?>
          <?php }?>
                    </div>
                     <div class="clear"></div>
                    <div align="center">
                      <div class="center__more__berita__rilis" id="more">Lihat lebih banyak berita</div>
                    </div>




                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 detail-page-rilis" id="sidebarrightkategori">
                <div class="ads-iklan-800 pad0"><img src="<?php echo $images;?>iklan-dupont-pioneer-870x725.jpg" alt="iklan" title="iklan" /></div>
                <div class="widget-title st-2-4">
                    <h3>POPULAR</h3>
                    <div class="separator-6"></div>
                </div>
                <div class="clear"></div>
                <?php $i=1; foreach ($populer as $pop){?>
                <div class="room__berita__popular"> 
                  <a href="<?php echo site_url(url_title($pop['urltitle']).'.html');?>">
                  <div class="in__room__berita__popular">
                    <div class="pull-left no__populer__rilis"><?php echo $i; ?></div>
                    <div class="pull-left berita__populer__rilis"><?php echo $pop['judul_artikel']; ?></div>
                    <div class="clear"></div>
                  </div>
                  </a>
                </div>
                  <?php $i++; } ?>
                 <div class="tag_footer_div"> 
              <?php $static = $this->T_halamanstatis->select();
              $count =  count($static );
              $i=1;
              foreach($static as $a){ ?>
          <a target="_blank" href="<?php echo site_url('page/'.url_title($a['titleurl']));?>">
            <div class="left_t_footer_n01"><?php echo strtoupper($a['judul_hs']);?></div>
          </a>
          <?php } ?>
        
          <div class="clear"></div>
        </div>
<!--         <div align="center">
  <div class="apps__center">
    <div class="pull-left apps"><img src="<?php //echo $images;?>google_play.png"></div>
    <div class="pull-left apps store"><img src="<?php //echo $images;?>appstore.png"></div>
    <div class="clear"></div>
  </div>
</div> -->
        <div align="center">
        <?php 
        $sosmed   = $this->T_informasi->get();?>
          <div class="social_media_footer"> <a href="http://<?php echo $sosmed['facebook'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-facebook fb"></i></div>
            </a> <a href="http://<?php echo $sosmed['twitter'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
            </a> <a href="http://<?php echo $sosmed['instagram'];?>" target="_blank">
            <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
            </a> <a href="http://<?php echo $sosmed['gplus'];?>" target="_blank">
            <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>
