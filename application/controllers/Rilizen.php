<?php 
class Rilizen extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_user');
		$this->load->model('T_user_detail');
		$this->load->model('T_artikel_rilizen');
		$this->load->model('T_artikel');
		$this->load->model('T_tag');
		$this->load->model('T_relasi');
		$this->load->model('T_komentar');
		$this->load->model('T_message_rilizen');
		$this->load->model('T_halamanstatis');
		$this->load->model('T_informasi');
		$this->load->model('T_fokus');
		
	}

	function index(){

		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		$data['page'] = 'Artikel';
		$data['title'] = 'Rilizen';
		$data['profile'] 	= 	$this->T_user->getid($this->session->userdata('id'));
		if(!empty($this->input->post('status')))
		{
			$data['artizen']	=	$this->T_artikel_rilizen->sortby($this->session->userdata('id'), $this->input->post('status'));
		}
		else
		{
			$data['artizen']	=	$this->T_artikel_rilizen->getdata($this->session->userdata('id'));
		}
		$data['status']		= 	$this->input->post('status');
		$data['allartikel']	=	$this->T_artikel->getidcount($this->session->userdata('id'));
		$data['allview']	=	$this->T_artikel->getpageview($this->session->userdata('id'));
		$data['sort']		=	$this->T_artikel->sort();
		$data['tag']		=	$this->T_tag->get();
		$data['tag2']		=	$this->T_tag->get();
		$this->load->view ('rilizen/index', $data);
	}

	function artrilizen()
		{
			$data['page'] = 'Rilizen';
			$data['title'] = 'Rilis';
			$data['all']	= $this->T_artikel_rilizen->getartikelmember();
			$this->load->view('rilizen/rilizen', $data);
		}

	function morerilizen($j)
		{
			$data['page'] = 'Rilizen';
			$data['title'] = 'Rilis';
			$data['all']	= $this->T_artikel_rilizen->getartikelmember($j);
			$this->load->view('rilizen/morerilizen', $data);
		}

	function profile(){

		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		$data['page'] = 'Profile';
		$data['title'] = 'Rilizen';
		$data['profile'] = $this->T_user->getid($this->session->userdata('id'));
		$data['allartikel']	=	$this->T_artikel->getidcount($this->session->userdata('id'));
		$data['allview']	=	$this->T_artikel->getpageview($this->session->userdata('id'));
		$data['sort']		=	$this->T_artikel->sort();
		$data['tag']		=	$this->T_tag->get();
		$this->load->view ('rilizen/profile', $data);
	}

	function editprofile()
	{

		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('upload_rilizen') ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = $this->input->post('image');
		}

		if(!empty($_FILES['banner']['name'])){
			$image_name_banner =  str_replace(' ','_',date('Ymdhis').$_FILES['banner']['name']);
			$config['upload_path'] 		= $this->config->item('upload_rilizen') ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name_banner;
			$this->upload->initialize($config);
			$this->upload->do_upload('banner');
		}else{
			$image_name_banner = $this->input->post('banner');
		}

		$data = array(
			'nama_lengkap'	=>	$this->input->post('nama'),
			'email'			=>	$this->input->post('email'));
		$this->T_user->edit($data, $this->session->userdata('id'));

		$data2 = array(
			'phone'			=>	$this->input->post('phone'),
			'image'			=>	$image_name,
			'banner'		=>	$image_name_banner,
			'birthday'		=>	$this->input->post('birthday'),
			'place'			=>	$this->input->post('place'),
			'facebook'		=>	$this->input->post('facebook'),
			'twitter'		=>	$this->input->post('twitter'),
			'instagram'		=>	$this->input->post('instagram'),
			'gplus'			=>	$this->input->post('gplus'),
			'address'		=>	$this->input->post('address'),
			'description'	=>	$this->input->post('description')
		);
		$this->T_user_detail->edit($data2, $this->session->userdata('id'));
		redirect('rilizen/profile');
	}

	function komentar(){

		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		$data['page'] = 'Komentar';
		$data['title'] = 'Rilizen';
		$data['profile'] 	= $this->T_user->getid($this->session->userdata('id'));
		$data['allartikel']	=	$this->T_artikel->getidcount($this->session->userdata('id'));
		$data['allview']	=	$this->T_artikel->getpageview($this->session->userdata('id'));
		$data['sort']		=	$this->T_artikel->sort();
		$data['tag']		=	$this->T_tag->get();
		$data['komentar']	=	$this->T_komentar->listkomentar($this->session->userdata('id'));
		$this->load->view ('rilizen/komentar', $data);
	}

	function message(){

		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		$data['page'] = 'Message';
		$data['title'] = 'Rilizen';
		$data['profile'] 	= $this->T_user->getid($this->session->userdata('id'));
		$data['allartikel']	=	$this->T_artikel->getidcount($this->session->userdata('id'));
		$data['allview']	=	$this->T_artikel->getpageview($this->session->userdata('id'));
		$data['sort']		=	$this->T_artikel->sort();
		$data['tag']		=	$this->T_tag->get();
		$data['message']	=	$this->T_message_rilizen->getid($this->session->userdata('id'));
		$this->load->view ('rilizen/message', $data);
	}

	function user_point(){

		$data['page'] = 'UserPt';
		$data['title'] = 'UserPt';
		$data['profile'] 	= $this->T_user->getid($this->session->userdata('id'));
		$data['allartikel']	=	$this->T_artikel->getidcount($this->session->userdata('id'));
		$data['allview']	=	$this->T_artikel->getpageview($this->session->userdata('id'));
		$data['sort']		=	$this->T_artikel->sort();
		$data['tag']		=	$this->T_tag->get();
		$data['populer']	=	$this->T_artikel->populer();
		//$data['page'] = 'Rilizen';
		$this->load->view ('rilizen/user_point', $data);
	}

	function user($id){

		$data['page'] = 'User';
		$data['title'] = 'User';
		$data['artizen2']	=	$this->T_artikel->getdata($id);
		$data['profile'] 	= $this->T_user->getid($id);
		$data['allartikel']	=	$this->T_artikel->getidcount($id);
		$data['allview']	=	$this->T_artikel->getpageview($id);
		$data['sort']		=	$this->T_artikel->sort();
		$data['tag']		=	$this->T_tag->get();
		$this->load->view ('rilizen/user', $data);
	}

	function addarticle()
	{
		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images');
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				//$config['min_width'] = '800';
				//$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}
		
		$data = array(
			'id_admin'			=>	$this->session->userdata('id'),
			'id_section'		=>	$this->input->post('kategori'),
			'judul_artikel'		=> 	$this->input->post('title'),
			'seo_artikel'		=>	'',
			'isi_artikel'		=>	$this->input->post('detail'),
			'gagasan_utama'		=>	$this->input->post('gagasan'),
			'kota'				=>	'',
			'hari'				=>	'',
			'tanggal'			=>	date('Y-m-d'),
			'jam'				=>	date('H:i:s'),
			'tgl_pub'			=>	date('Y-m-d H:i:s'),
			'dibaca'			=>	'',
			'meta_key'			=>	'',
			'meta_des'			=>	'',
			'headline'			=>	'',
			'hot'				=>	'N',
			'publish'			=>	'N',
			'sponsored'			=>	'N',
			'fokus'				=>	'',
			'kolom'				=>	'',
			'thumbnail'			=>	$image_name,
			'ket_thumbnail'		=>	'',
			'id_video'			=>	'',
			'comment'			=>	'',
			'url'				=>	$this->input->post('video'),
			'parent_id'			=>	'',
			'postdate'			=>	date('Y-m-d H:i:s'),
			'apps'				=>	'',
			'oldid'				=>	'',
			'oldpenulis'		=>	'',
			'oldeditor'			=>	'',
			'oldtags'			=>	'',
			'oldimage'			=>	'',
			'thumb'				=>	'',
			'slider'			=>	'',
			'position'			=>	'',
			'urltitle'			=>	url_title($this->input->post('title'))
		);

		//print_r($data);
		$this->T_artikel_rilizen->add($data);

		$lastid = $this->db->insert_id();
		
		foreach($this->input->post('tag') as $tag)
		{
			$data2 = array(
				'id_object'    	  	  	=> $lastid,
				'id_object2'	    	=> $tag,
				'tipe'     				=> 'art_tag'
			);
			$this->T_relasi->add($data2);
		}
		
		redirect('rilizen');

	}
	function artikel($id)
	{
		$data['page'] = 'User';
		$data['title'] = 'User';
		$related = $this ->T_artikel->get_artikel($id);
		$data['detail'] = $this ->T_artikel_rilizen->get_artikel($id);
		$data['recent'] = $this->T_artikel->recent(5,0);
		$data['terkait'] = $this->T_artikel->related($related['id_section']);
		$data['tags'] = $this->T_tag->tag($related['id_artikel']);
		$this->load->view('detail/artikel_rilizen', $data);
	}


	function editarticle()
	{
		if(!empty($this->session->userdata('nama_lengkap')))
		{
			'';
		}
		else
		{
			redirect('login');
		}
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images');
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[0]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[1]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[2]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				//$config['min_width'] = '800';
				//$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}
		
		$data = array(
			'id_section'		=>	$this->input->post('kategori'),
			'judul_artikel'		=> 	$this->input->post('title'),
			'isi_artikel'		=>	$this->input->post('detail'),
			'gagasan_utama'		=>	$this->input->post('gagasan'),
			'thumbnail'			=>	$image_name,
			'id_video'			=>	$this->input->post('video'),
			'urltitle'			=>	url_title($this->input->post('title'))
		);

		//echo $hari;
		//print_r($data);
		$this->T_artikel_rilizen->update($data, $this->input->post('id'));
		
		/*
		$lastid = $this->db->insert_id();
		
		foreach($this->input->post('tag') as $tag)
		{
			$data2 = array(
				'id_object'    	  	  	=> $lastid,
				'id_object2'	    	=> $tag,
				'tipe'     				=> 'art_tag'
			);
			$this->T_relasi->add($data2);
		}
		*/
		
		redirect('rilizen');

	}

}