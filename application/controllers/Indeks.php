<?php
class Indeks extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		$this->load->model('T_informasi');
		$this->load->model('T_halamanstatis');
	}

	function index(){

		$data['page'] = 'Home/allartikel';
		$data['title'] = 'Rilis';
		if(!empty($this->input->post('filter')))
		{
			$data['all']	=	$this->T_artikel->sortby(12, $this->input->post('filter'));
		}
		else
		{
			$data['all']	=	$this->T_artikel->select();
		}
		$data['filter']		= 	$this->input->post('filter');
		$data['populer'] = $this->T_artikel->populer();
		//$data['all'] = $this->T_artikel->select();
		$this->load->view ('indeks/index', $data);
	}

	function more($j,$id=0){
		if(!empty($id))
		{
			$data['more']	=	$this->T_artikel->moreq(12, 12, $id);
		}
		else
		{
			$data['more']	=	$this->T_artikel->morex(12, $j);
		}

		$this->load->view ('indeks/more', $data);
	}
}

?>
