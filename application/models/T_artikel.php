<?php 
class T_artikel extends CI_Model
{
	protected $_table = 't_artikel';

	function select($limit=13){
		
		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('publish', 'Y');
		$this->db->limit($limit);
		$this->db->order_by('tgl_pub','desc');
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}

	function getchildfoto($id)
	{
		$this->db->where('parent_id', $id);
		$this->db->where('publish', 'Y');
		return $this->db->get($this->_table)->result_array();
	}

	function anak_foto($id,$limit=3)
	{
		$this->db->where('parent_id', $id);
		$this->db->where('publish', 'Y');
		$this->db->limit($limit);
		return $this->db->get($this->_table)->result_array();
	}

	function getfoto($limit=10,$offset=0)
	{
		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  		$this->db->where('t_artikel.id_section', 140);
  		$this->db->where('t_artikel.parent_id', 0);
  		$this->db->where('t_artikel.publish', 'Y');
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('id_artikel', 'DESC');
		return $this->db->get($this->_table)->result_array();
		
	}

	function getfotomore($limit,$offset)
	{
		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  		$this->db->where('t_artikel.id_section', 140);
  		$this->db->where('t_artikel.publish', 'Y');
  		$this->db->or_where('t_artikel.id_section', 130);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('id_artikel', 'DESC');
		return $this->db->get($this->_table)->result_array();
		
	}

	function sortby($limit=12, $id)
	{
		$this->db->limit($limit);
		$this->db->join('t_section', 't_section.id_section = t_artikel.id_section');
		$this->db->where('t_section.id_section', $id);
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->order_by('t_artikel.tgl_pub', 'DESC');
		return $this->db->get($this->_table)->result_array();
	}

	function recent($limit=9){

		$this->db->select('*');
		$this->db->join('t_section','t_artikel.id_section = t_section.id_section');
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->limit($limit);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->where_not_in('t_artikel.id_section', array(140,141,143,144,145,148));
		return $this->db->get_where('t_artikel')->result_array();
	}

	function get_populer($limit=5)
	{
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->order_by('tgl_pub','desc' );
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->limit($limit);
		return $this->db->get($this->_table)->result_array();
	}

	function populer()
	{
		$this->db->limit(5);
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->where('tgl_pub >=', date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m") , date("d")-30, date("Y"))));
		$this->db->where('daerah', 1);
		$this->db->where('publish', 'Y');
		$this->db->order_by('dibaca', 'desc');
		$this->db->where_not_in('id_section', array(140,141,143,144,145,148));
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}

	function get_foto($id)
	{
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->order_by('tgl_pub','desc');
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->where('t_artikel.id_section', $id); 
		return $this->db->get($this->_table)->result_array();
	}

	function get_fotopopuler($id,$limit=4)
	{
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->order_by('dibaca','desc');
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->where('t_artikel.id_section', $id); 
		$this->db->limit($limit);
		return $this->db->get($this->_table)->result_array();
	}

	// function getatas($id)
	// {
	// 	$this->db->select('*');
	// 	$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
	// 	$this->db->order_by('tgl_pub','desc');
	// 	$this->db->where('t_artikel.id_section', $id); 
	// 	return $this->db->get($this->_table)->result_array();
	// }

	function get_fotolain($id,$limit=24)
	{
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->order_by('tgl_pub','desc');
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->where('t_artikel.id_section', $id);
		$this->db->offset(6); 
		$this->db->limit($limit);
		return $this->db->get($this->_table)->result_array();
	}

	function get_nasional($id,$limit=4)
	{
		$this->db->select('*');
		$this->db->join('t_section','t_artikel.id_section = t_section.id_section');
		$this->db->order_by('t_artikel.tgl_pub','desc');
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->where('t_artikel.id_section', $id);
		$this->db->offset(1);
		$this->db->limit($limit);
		return $this->db->get($this->_table)->result_array();
	}

	function get_beritalain($id,$limit=15)
	{
		$this->db->select('*');
		$this->db->join('t_section','t_artikel.id_section = t_section.id_section');
		$this->db->order_by('t_artikel.tgl_pub','desc');
		$this->db->where('t_artikle.publish', 'Y');
		$this->db->where('t_artikel.id_section', $id);
		$this->db->limit($limit);
		return $this->db->get($this->_table)->result_array();
	}

	function pilihan($limit=4)
	{
		$this->db->select('t_artikel.oldimage, t_artikel.postdate,t_artikel.judul_artikel,t_artikel.isi_artikel, t_section.nama_section,t_artikel.urltitle,t_section.id_supsection,t_section.daerah,t_artikel.daerah,t_artikel.thumbnail');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->where('id_supsection', 133);
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->limit($limit);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->where('t_artikel.hot', 'Y');
		return $this->db->get_where($this->_table)->result_array();
	}

	function foto($limit=5){
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->where('id_supsection', 133);
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->limit($limit);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->where('t_artikel.id_section', 140);
		return $this->db->get_where($this->_table)->result_array();
	}

	function videoatas($id){
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		// $this->db->limit($limit);
		$this->db->order_by('tgl_pub', 'DESC');
		$query = $this->db->get_where('t_artikel', array('t_artikel.urltitle'=> $id));
		return $query->row_array();
	}

	function list_tag($id)
	{
		$this->db->select('*');
		$this->db->join('t_relasi', 't_artikel.id_artikel = t_relasi.id_object');
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->limit(8);
		$this->db->order_by('tgl_pub','desc');
		return $this->db->get($this->_table)->result_array();
	}

	function video($limit=4){
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->limit($limit);
		$this->db->where('t_artikel.id_section', 126);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('publish', 'Y');
		$this->db->order_by('tgl_pub', 'DESC');
		return $this->db->get_where($this->_table)->result_array();
	}

	function videolain($limit=5){
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->limit($limit);
		$this->db->where('t_artikel.id_section', 126);
		$this->db->order_by('tgl_pub', 'DESC');
		return $this->db->get_where($this->_table)->result_array();
	}


	function getmenu($id,$limit=3,$offset=0)
	{
		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('t_section.daerah', 1);
  		$this->db->where('t_artikel.daerah', 1);
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('tgl_pub', 'DESC');
		return $this->db->get_where($this->_table, array('t_artikel.id_section' => $id))->result_array();
	}

	function getmenumore($id,$limit=10,$offset=0)
	{
		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  		$this->db->where('publish', 'Y');
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('tgl_pub', 'DESC');
		return $this->db->get_where($this->_table, array('t_artikel.id_section' => $id))->result_array();
	}

	function hot_video($limit){
  		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->limit($limit);
		$this->db->where('publish', 'Y');
		$this->db->where('t_artikel.id_section', 141);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->order_by('dibaca','desc');
		return $this->db->get_where($this->_table)->result_array();
	}

	function headline($limit=3)
	{
		$this->db->select('*');
		$this->db->join('t_section', 't_section.id_section = t_artikel.id_section');
		$this->db->limit($limit);
		$this->db->where('headline', 'Y');
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->order_by('tgl_pub', 'desc');
		$query = $this->db->get('t_artikel');
		return $query->result_array();

	}

	function quotes($id)
	{
		$this->db->where('publish', 'Y');
		$this->db->where('id_section', $id);
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->order_by('tgl_pub', 'desc');
		return $this->db->get($this->_table)->row_array();
	}

	function inspirasi()
	{
		$this->db->select('*');
		$this->db->limit(5);
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->join('t_admin', 't_artikel.id_admin = t_admin.id_adm');
		//$this->db->where('kolom' != 0);
		$this->db->where('t_artikel.id_section', 14);
		return $this->db->get('t_artikel')->result_array();
	}

	function beritalainnya($limit=9, $offset=3){

		$this->db->select('*');
		$this->db->join('t_section','t_artikel.id_section = t_section.id_section');
		$this->db->where('publish', "Y");
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->where_not_in('t_artikel.id_section', array(140,141,143,144,145,148));
		$this->db->order_by('tgl_pub', 'DESC');
		return $this->db->get_where('t_artikel')->result_array();
	}

	function agenda($limit,$date_agenda)
	{
		$this->db->select('*');
		$this->db->where('tanggal =',$date_agenda);
		$this->db->limit(5);
		return $this->db->get('t_agenda')->result_array();
	}

	function kategori2($id)
	{
		$this->db->select('*');
		return $this->db->get_where('t_section', array('url_title' => $id, 'daerah' => 1))->row_array();
	}

	function search($keyword,$offset=0)
	{
	$this->db->like('judul_artikel', $keyword);
	$this->db->limit(3);
	$this->db->offset($offset);
  	$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  	$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  	$query = $this->db->get('t_artikel');
	return $query->result_array();
		/*$this->db->select('*');
		$this->db->limit($limit);
		$this->db->like('judul_artikel', $keyword);
		$this->db->join('t_section', 't_section.id_section = t_artikel.id_section');
		return $this->db->get('t_artikel')->result_array();*/

	}

	function sort()
	{
		$this->db->select('*');
		$this->db->select_sum('dibaca');
		$this->db->join('t_user', 't_user.id = t_artikel.id_admin');
		$this->db->join('t_user_detail', 't_user.id = t_user_detail.user_id');
		$this->db->join('t_section', 't_section.id_section = t_artikel.id_section');
		//$this->db->where('t_artikel_rilizen.id_admin', $id);
		$this->db->where('t_artikel.rilizen', 'Y');
		$this->db->group_by('t_artikel.id_admin');
		$this->db->order_by('dibaca', 'DESC');
		return $this->db->get($this->_table)->result_array();
		//print_r($this->db->last_query());
		//die();
	}

	function get_test($id)
	{
		//$this->db->select('*');
		//$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$query = $this->db->get_where('t_artikel', array('t_artikel.urltitle' => $id));
		return $query->row_array();
	}

	function get_artikel($id)
	{
		$this->db->select('*');
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->where('t_artikel.daerah', 1);
		$this->db->where('t_section.daerah', 1);
		$this->db->where('publish', 'Y');
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$query = $this->db->get_where('t_artikel', array('t_artikel.urltitle' => $id));
		//print_r($this->db->last_query());
  		//die();
		return $query->row_array();
	}

	function related($id)
	{
		$this->db->select('*');
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->where('t_artikel.publish', 'Y');
		$this->db->where('t_artikel.id_section', $id);
		$this->db->order_by('tgl_pub','random');
		$this->db->limit(5);
		return $this->db->get('t_artikel')->result_array();
	}

	function kolom($id)
	{
		$this->db->select('t_artikel.*, t_section.id_section, t_section.nama_section, t_kolom.image, t_kolom.nama, t_kolom.category_id');
		$this->db->limit(5);
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->where('t_artikel.id_section', $id);
		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
		$this->db->join('t_kolom', 't_artikel.id_section = t_kolom.category_id');
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}

	function get_fokus_detail($id)
	{
  		$this->db->join('t_fokus', 't_artikel.fokus = t_fokus.id');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->order_by('tgl_pub', 'DESC');
		return $this->db->get_where($this->_table, array('t_artikel.urltitle' => $id))->row_array();
	}

	function populer_kanan($limit){
  		$this->db->select('*');
		$section = array('2','126','127','128','129','130','131');
		$this->db->join('t_section','t_artikel.id_section = t_section.id_section');
		$this->db->limit($limit);
		$this->db->where('publish','Y');
		// $this->db->where('hot','Y');
		$this->db->where('tgl_pub >=', date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m") , date("d")-3, date("Y"))));
		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->order_by('dibaca','desc');
		$this->db->where_not_in('t_artikel.id_section', $section);
		$query 	=	$this->db->get($this->_table);
		return $query->result_array();
		/*print_r($this->db->last_query());
		die();*/
	}

	function hot_foto($limit,$offset){
  		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  		$this->db->where('t_artikel.id_section', 140);
  		$this->db->where('publish', 'Y');
  		$this->db->or_where('t_artikel.id_section', 130);
  		$this->db->where('t_artikel.parent_id', 0);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('t_artikel.dibaca', 'DESC');
		return $this->db->get($this->_table)->result_array();
		
	}

	function getessay($limit=10,$offset=0)
	{
		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  		$this->db->where('t_artikel.id_section', 130);
  		$this->db->where('publish', 'Y');
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('id_artikel', 'DESC');
		return $this->db->get($this->_table)->result_array();
		
	}

	function hot_essay($limit,$offset){
  		$this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  		$this->db->where('t_artikel.id_section', 130);
  		$this->db->where('publish', 'Y');
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('t_artikel.dibaca', 'DESC');
		return $this->db->get($this->_table)->result_array();
		
	}

	function morex($limit=12, $offset=12)
    {
  	    $this->db->select('*');
  		$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  		$this->db->where('publish', 'Y');
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('tgl_pub','desc');
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}

	function moreq($limit=12,$offset=12,$id)
	{
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->join('t_section', 't_section.id_section = t_artikel.id_section');
		$this->db->where('t_section.id_section', $id);
		$this->db->where('publish', 'Y');
		$this->db->order_by('t_artikel.tgl_pub', 'DESC');
		return $this->db->get($this->_table)->result_array();
	}
	function search_more($keyword,$limit=3,$offset=0)
  	{
  	$this->db->like('judul_artikel', $keyword);
  	$this->db->limit($limit);
  	$this->db->offset($offset);
  	$this->db->join('t_section', 't_artikel.id_section = t_section.id_section');
  	$this->db->where('tgl_pub <=', date('Y-m-d H:i:s'));
  	$query = $this->db->get('t_artikel');

  	return $query->result_array();
  }
}