<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<section id="kategori-page-rilis">
	<div class="container">
    	<div class="row col-lg-9-3-detail-page">
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 detail-page-rilis">  
       		<div class="room_center_slider_headline"> 
          <!--content-->
          <div class="sub_menu_page_detail">
            <div class="nama_sub0001"><a href="index.html">HOME</a></div>
            <div class="right_sub"><i class="fa fa-angle-double-right angle_0111"></i></div>
            <div class="nama_sub0001"><a href="kategori-foto.html">SEARCH RESULT</a></div>
            <div class="right_sub"><i class="fa fa-angle-double-right angle_0111"></i></div>
            <div class="nama_sub0001"><a href="">LOREM IPSUM SIT DOLOR AME</a></div>
            <div class="clear"></div>
          </div>

<div class="row berita__lainnya">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-01.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-01.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Sosok</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-02.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-02.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-03.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-03.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Opini</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-04.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-04.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Daerah</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-05.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-05.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Dunia</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-06.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-06.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-04.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-04.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Daerah</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-05.jpg" alt="berita" title="berita" src="<?php echo $images; ?>/img-terkini-05.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Dunia</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-06.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-06.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-01.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-01.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Sosok</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-02.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-02.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-03.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-03.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Opini</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-04.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-04.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Daerah</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-05.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-05.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Dunia</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-06.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-06.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-04.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-04.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Daerah</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-05.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-05.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Dunia</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-06.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-06.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-01.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-01.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Sosok</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-02.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-02.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-03.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-03.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Opini</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-04.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-04.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Daerah</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-05.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-05.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Dunia</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-06.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-06.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-04.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-04.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Daerah</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-05.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-05.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Dunia</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
            <div class="entry-item">
              <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="image/img-terkini-06.jpg" alt="berita" title="berita" src="<?php echo $images; ?>img-terkini-06.jpg" style="display: inline;" width="800" height="496"> </a> </div>
              <div class="entry-box">
                <header class="entry-header">
                  <div class="entry-meta"> <span class="entry-date">12/11/2017, 17.00 WIB </span></div>
                  <h4 class="entry-title st-4-1"> <a class="s41" href="#">Lorem Ipsum has been the industry's standard dummy text</a> </h4>
                  <a href="kategori.html">
                  <div class="tag__berita__laiinya">Nasional</div>
                  </a> </header>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          <div align="center"> <a href="more.html">
            <div class="center__more__berita__rilis">Lihat lebih banyak berita</div>
            </a> </div>
          <br>
        </div>          



 
        </div>         
       </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 detail-page-rilis" id="sidebarrightkategori">
        <div class="ads-iklan-800 pad0"><img src="<?php echo $images; ?>iklan-dupont-pioneer-870x725.jpg" alt="iklan" title="iklan"/></div>
        <div class="widget-title st-2-4">
          <h3>POPULAR</h3>
          <div class="separator-6"></div>
        </div>
        <div class="clear"></div>
           <div class="room__berita__popular"> 
           <a href="">
          <div class="in__room__berita__popular">
            <div class="pull-left no__populer__rilis">1</div>
            <div class="pull-left berita__populer__rilis">Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
            <div class="clear"></div>
          </div>
          </a> <a href="">
          <div class="in__room__berita__popular">
            <div class="pull-left no__populer__rilis">2</div>
            <div class="pull-left berita__populer__rilis">Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
            <div class="clear"></div>
          </div>
          </a> <a href="">
          <div class="in__room__berita__popular">
            <div class="pull-left no__populer__rilis">3</div>
            <div class="pull-left berita__populer__rilis">Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
            <div class="clear"></div>
          </div>
          </a> <a href="">
          <div class="in__room__berita__popular">
            <div class="pull-left no__populer__rilis">4</div>
            <div class="pull-left berita__populer__rilis">Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
            <div class="clear"></div>
          </div>
          </a> <a href="">
          <div class="in__room__berita__popular">
            <div class="pull-left no__populer__rilis">5</div>
            <div class="pull-left berita__populer__rilis">Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
            <div class="clear"></div>
          </div>
          </a> </div>
          <div class="tag_footer_div"> <a href="">
          <div class="left_t_footer_n01">REDAKSI</div>
          </a> <a href="">
          <div class="left_t_footer_n01">PEDOMAN</div>
          </a> <a href="">
          <div class="left_t_footer_n01">DISCLAMER</div>
          </a> <a href="">
          <div class="left_t_footer_n01">KODE ETIK</div>
          </a> <a href="">
          <div class="left_t_footer_n01">HISTORY RILIS</div>
          </a> <a href="">
          <div class="left_t_footer_n01 GONE_PAD_RIGHT">KONTAK</div>
          </a>
          <div class="clear"></div>
        </div>
        <div align="center">
          <div class="apps__center">
            <div class="pull-left apps"><img src="<?php echo $images; ?>google_play.png"></div>
            <div class="pull-left apps store"><img src="<?php echo $images; ?>appstore.png"></div>
            <div class="clear"></div>
          </div>
        </div>
        <div align="center">
          <div class="social_media_footer"> <a href="">
            <div class="my_social"><i class="fa fa-facebook fb"></i></div>
            </a> <a href="">
            <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
            </a> <a href="">
            <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
            </a> <a href="">
            <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
           
        </div>
        <div class="clear"></div>
    </div>
    </div>
</section>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>