<?php
class T_halamanstatis extends CI_Model 
{
	protected $_table = 't_halamanstatis';
	function __construct()
	{
		parent::__construct();
	}
	
	function select(){
		$this->db->order_by('position', 'ASC');
		$this->db->where('daerah', 1);
		$query = $this->db->get('t_halamanstatis');
		return $query->result_array();
	}


	function get($id)
		{
			$query   = $this->db->get_where($this->_table, array('titleurl' => $id));
			return $query->row_array();
		}
}