<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HAuth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_user');
		$this->load->model('T_user_detail');
	}

	public function index()
	{
		$this->load->view('hauth/home');
	}

	public function login($provider)
	{
		log_message('debug', "controllers.HAuth.login($provider) called");

		try
		{
			log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');
			$this->load->library('HybridAuthLib');

			if ($this->hybridauthlib->providerEnabled($provider))
			{
				log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
				$service = $this->hybridauthlib->authenticate($provider);

				if ($service->isUserConnected())
				{
					log_message('debug', 'controller.HAuth.login: user authenticated.');

					$user_profile = $service->getUserProfile();

					log_message('info', 'controllers.HAuth.login: user profile:'.PHP_EOL.print_r($user_profile, TRUE));

					//$data['user_profile'] = $user_profile;
					//print_r($data['user_profile']);
					//echo '<br>';

					$cek = $this->T_user->ceksosmed($user_profile->identifier);

					if(!empty($cek))
					{
						$data_session = array(
							'id'			=> 	$cek['id'],
							'nama_lengkap'	=> 	$cek['nama_lengkap']
										);
						$this->session->set_userdata($data_session);
						redirect('rilizen');
					}
					else
					{
						if(!empty($user_profile->email))
						{
							$email = $user_profile->email;
						}
						else
						{
							$email = '';
						}

						
						if(!empty($user_profile->phone))
						{
							$phone = $user_profile->phone;
						}
						else
						{
							$phone = '';
						}

						if(!empty($user_profile->address))
						{
							$address = $user_profile->address;
						}
						else
						{
							$address = '';
						}

						if(!empty($user_profile->phone))
						{
							$phone = $user_profile->phone;
						}
						else
						{
							$phone = '';
						}

						if(!empty($user_profile->description))
						{
							$description = $user_profile->description;
						}
						else
						{
							$description = '';
						}

						if(!empty($user_profile->region))
						{
							$region = $user_profile->region;
						}
						else
						{
							$region = '';
						}

						if($provider == 'Google')
						{
							if(!empty($user_profile->profileURL))
							{
								$google = $user_profile->profileURL;
							}
							else
							{
								$google = '';
							}
						}
						else
						{
							$google = '';
						}

						if($provider == 'Twitter')
						{
							if(!empty($user_profile->profileURL))
							{
								$twitter = $user_profile->profileURL;
							}
							else
							{
								$twitter = '';
							}
						}
						else
						{
							$twitter = '';
						}


						if($provider == 'Facebook')
						{
							if(!empty($user_profile->profileURL))
							{
								$facebook = $user_profile->profileURL;
							}
							else
							{
								$facebook = '';
							}
						}
						else
						{
							$facebook = '';
						}


						$user = array(
							'nama_lengkap'	=>	$user_profile->displayName,
							'email'			=>	$email,
							'password'		=>	'',
							'profesi'		=>	'',
							'poin'			=>	'',
							'salt'			=>	'',
							'id_sosmed'		=>	$user_profile->identifier);
						$this->T_user->add($user);
						$id = $this->db->insert_id();
						$userdetail = array(
							'user_id'		=>	$id,
							'phone'			=>	$phone,
							'image'			=>	$user_profile->photoURL,
							'banner'		=>	'',
							'birthday'		=>	'0000-00-00',
							'place'			=>	$region,
							'facebook'		=>	$facebook,
							'twitter'		=>	$twitter,
							'gplus'			=>	$google,
							'address'		=>	$address,
							'description'	=>	$description);
						$this->T_user_detail->add($userdetail);
						$data_session = array(
							'id'			=> 	$id,
							'nama_lengkap'	=> 	$user_profile->displayName
										);
						$this->session->set_userdata($data_session);
						redirect('rilizen');
					}
					//$this->load->view('hauth/done',$data);
				}
				else // Cannot authenticate user
				{
					show_error('Cannot authenticate user');
				}
			}
			else // This service is not enabled.
			{
				log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
				show_404($_SERVER['REQUEST_URI']);
			}
		}
		catch(Exception $e)
		{
			$error = 'Unexpected error';
			switch($e->getCode())
			{
				case 0 : $error = 'Unspecified error.'; break;
				case 1 : $error = 'Hybriauth configuration error.'; break;
				case 2 : $error = 'Provider not properly configured.'; break;
				case 3 : $error = 'Unknown or disabled provider.'; break;
				case 4 : $error = 'Missing provider application credentials.'; break;
				case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
				         //redirect();
				         if (isset($service))
				         {
				         	log_message('debug', 'controllers.HAuth.login: logging out from service.');
				         	$service->logout();
				         }
				         show_error('User has cancelled the authentication or the provider refused the connection.');
				         break;
				case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
				         break;
				case 7 : $error = 'User not connected to the provider.';
				         break;
			}

			if (isset($service))
			{
				$service->logout();
			}

			log_message('error', 'controllers.HAuth.login: '.$error);
			show_error('Error authenticating user.');
		}
	}

	public function endpoint()
	{

		log_message('debug', 'controllers.HAuth.endpoint called.');
		log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
			$_REQUEST = $_GET;
		}

		log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
		require_once APPPATH.'/third_party/hybridauth/index.php';

	}
}

/* End of file hauth.php */
/* Location: ./application/controllers/hauth.php */
