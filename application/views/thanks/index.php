<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<div class="container" style="display:none;">
  <div class="nm__daerah">
    <div class="pull-left nama_daerah">Nasional</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Lampung</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Bandung</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jawa Tengah</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jogya</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Malang</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Jawa Timur</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Makassar</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-left nama_daerah">Papua</div>
    <div class="pull-left line__daerah"></div>
    <div class="pull-right nama_daerah">Kota Lainnya <i class="fa fa-angle-down angle-down"></i></div>
    <div class="clear"></div>
  </div>
</div>
<section id="inside-page-rilis">
  <div class="container">
    <div class="row row-inside-page">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 inside-page-col">
        <div class="room-page-inside-rilis"> 
      <?php $static = $this->T_halamanstatis->select();
              $count =  count($static );
              $i=1;
              foreach($static as $a){ ?>
        <a href="<?php echo site_url('page/'.$a['titleurl']);?>">
          <div class="page-in-inside-room"><?php echo strtoupper($a['judul_hs']);?></div>
          </a> 
        <?php } ?>
         </div>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 inside-page-col">
        <div class="room__title__kategori-inside"> 
           <a href="#">
          <div class="pull-left left__orange__big"><h1>HALAMAN |</h1></div>
          </a>
          <a href="#">
          <div class="sub__title__menu__kategori pull-left" style="position: relative; top:25px"><h4>THANK</h4></div>
          </a>
          <div class="clear"></div>
        </div>
        <div class="room-text-inside-redaksi-01">
        <div class="img-inside-100">
          <P>EMAIL ANDA SUDAH TERKIRIM, SEDANG KAMI PROSES</P>
        TERIMA KASIH
        </div>
        
      </div>
      
      <div class="clear"></div>
    </div>
  </div>
</section>
</body>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>