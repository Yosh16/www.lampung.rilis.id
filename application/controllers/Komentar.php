<?php 
class Komentar extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_reaksi');
		$this->load->model('T_komentar');
		$this->load->model('T_admin');
		$this->load->model('T_reaksi_log');
		$this->load->model('T_fokus');
		$this->load->model('T_halamanstatis');
		$this->load->model('T_informasi');
		$this->load->model('T_user');
	}

	function index($id){
		
		$data['page'] = 'HKomentar';
		$data['title'] = 'Rilis';
		$data['detail'] = $this ->T_artikel->detail_komentar($id);
		$related = $this ->T_artikel->get_artikel($id);
		$data['recent'] = $this->T_artikel->recent(5,0);
		$data['terkait'] = $this->T_artikel->related($related['id_section']);
		$data['reaksi']	= $this->T_reaksi->getid($id);
		$data['fokus'] 				= $this->T_fokus->getfokus();
		$data['komentar'] = $this->T_komentar->getkomentarall($related['id_artikel']);
		$data['seksi']	=	$related['id_section'];
		$this->load->view ('komentar/index', $data);
	}


	function addaction()
	{
		if(!empty($this->session->userdata('nama_lengkap')))
		{
			$artikel = $this->T_artikel->getid($this->input->post('id'));
			$data = array(
				'id_artikel'	=>	$this->input->post('id'),
				'komentar'		=>	$this->input->post('komentar'),
				'id_user'		=>	$this->session->userdata('id'),
				'postdate'		=>	date('Y-m-d H:i:s'));
			$this->T_komentar->add($data);
			redirect('komentar/'.$artikel['urltitle']);
		}
		else
		{
			redirect('login/index/'.$this->input->post('id').'-auth');
		}
	}
}