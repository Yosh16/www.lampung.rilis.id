<?php

/**
* 
*/
class T_komentar extends CI_Model
{
	
	protected $_table = 't_komentar';

	function getkomentar($id)
	{
		$this->db->join('t_user', 't_user.id = t_komentar.id_user');
		$this->db->join('t_user_detail', 't_komentar.id_user = t_user_detail.user_id');
		$this->db->where('id_artikel', $id);
		$this->db->limit(3);
		return $this->db->get($this->_table)->result_array();
	}

	function getkomentarall($id)
	{
		$this->db->join('t_user', 't_user.id = t_komentar.id_user');
		$this->db->join('t_user_detail', 't_komentar.id_user = t_user_detail.user_id');
		$this->db->where('id_artikel', $id);
		return $this->db->get($this->_table)->result_array();
	}

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function listkomentar($id)
	{
		$this->db->join('t_artikel', 't_artikel.id_artikel = t_komentar.id_artikel');
		$this->db->where('id_user', $id);
		$this->db->group_by('t_komentar.id_artikel');
		return $this->db->get($this->_table)->result_array();
		//print_r($this->db->last_query());
		//die();
	}

	function komentar($id)
	{
		$this->db->where('id_artikel', $id);
		return $this->db->get($this->_table)->result_array();
	}
}