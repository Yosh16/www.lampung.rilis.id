<?php
class T_section extends CI_Model 
{
	protected $_table = 't_section';

	function getsupsection($id)
	{
		return $this->db->get_where($this->_table, array('id_supsection' => $id,'status' => 1, 'daerah' => 1))->result_array();
	}

	function countchild($data)
	{
		$this->db->where('id_supsection', $data);
		$this->db->where('daerah', 1);
		$this->db->from($this->_table);
		return $this->db->count_all_results();
	}

	function getcategory($id)
	{
		$this->db->order_by('position', 'ASC');
		return $this->db->get_where($this->_table, array('id_supsection' => $id, 'daerah' => 1))->result_array();
	}

	function getsectionindeks()
	{
		$this->db->select('*');
		$this->db->where('id_supsection', 133);
		$this->db->where('status', 1);
		$this->db->where('daerah', 1);
		$this->db->where_not_in('id_section', 147);
		$this->db->order_by('position', 'ASC');
		return $this->db->get($this->_table)->result_array();
	}

	function getsection()
	{
		$this->db->select('*');
		$this->db->where('id_supsection', 133);
		$this->db->where('status', 1);
		$this->db->where('daerah', 1);
		$this->db->order_by('position', 'ASC');
		return $this->db->get($this->_table)->result_array();
	}

	function section($id)
	{
		$this->db->where('status', 1);
		$this->db->where('daerah', 1);
		$this->db->where('id_section', $id);
		return $this->db->get($this->_table)->row_array();
	}
	function sectionbread($id)
	{
		$this->db->where('id_section', $id);
		return $this->db->get($this->_table)->row_array();
	}


}
?>