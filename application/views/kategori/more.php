<?php 
$base             = $this->config->item('base_url');
$css              = $this->config->item('css');
$images           = $this->config->item('images');
$font             = $this->config->item('font');
$js               = $this->config->item('js');
$fonts            = $this->config->item('fonts');
$images_upload    = $this->config->item('images_front');
$upload_images    = $this->config->item('upload_images');
$tim              = $this->config->item('images_tim');
$images_upload    = $this->config->item('images_upload');
$upload           = $this->config->item('upload');
$rilizen           = $this->config->item('rilizen');
$mimin            = $this->config->item('mimin');
?>
<?php foreach($lainnya as $a){ ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
              <div class="entry-item">
              <?php  $path = date('Y/m/d/', strtotime($a['postdate']));
              if(!empty($a['thumbnail'])) {
                $mystring = $a['thumbnail'];
                $findme   = 'http';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {?>
                <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>"> <img class="lazy img_responsive_100" src="<?php echo $tim.$upload.$path.$a['thumbnail'];?>&w=800&h=496&zc=1" alt="berita" title="berita"> </a> </div>
                <?php }else{ ?>
                <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>"> <img class="lazy img_responsive_100" src="<?php echo $tim.$a['thumbnail'];?>&w=800&h=496&zc=1" alt="berita" title="berita"> </a> </div>
                <?php }?>
                <?php ;} else {?>
                <div class="entry-thumb"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>"> <img class="lazy img_responsive_100" src="<?php echo $tim.$a['oldimage'];?>&w=800&h=496&zc=1" alt="berita" title="berita"> </a> </div>
                <?php } ?>
                <div class="entry-box">
                  <header class="entry-header">
                    <div class="entry-meta"> <span class="entry-date"><?php
                    $ex = explode(' ', $a['tgl_pub']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?> </span></div>
                    <h4 class="entry-title st-4-1"> <a class="s41" href="<?php echo site_url(url_title($a['urltitle']).'.html')?>"><?php echo substr(($a['judul_artikel']),0,34);?></a> </h4>
                    <a href="<?php echo site_url('kategori/'.url_title($a['url_title']))?>">
                    <div class="tag__berita__laiinya"><?php echo $a['nama_section'];?></div>
                    </a> </header>
                </div>
              </div>
            </div>
          <?php } ?>