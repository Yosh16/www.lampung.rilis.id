<?php
/**
* 
*/
class T_fokus extends CI_Model
{
	
	protected $_table = 't_fokus';

	function getfokus()
	{
		$this->db->order_by('postdate', 'random');
		return $this->db->get($this->_table)->row_array();
	}

	function fokus()
	{
		$this->db->select('*');
		$this->db->join('t_artikel', 't_fokus.id = t_artikel.fokus');
		$this->db->join('t_admin', 't_fokus.post_by = t_admin.id_adm');
		$this->db->where('t_artikel.tgl_pub <=', date('Y-m-d H:i:s'));
		$this->db->order_by('t_fokus.postdate', 'desc');
		$query = $this->db->get($this->_table);
		return $query->row_array();
	}

	function getbig($limit)
	{
		$this->db->limit($limit);
		//$this->db->join('t_artikel', 't_fokus.id = t_artikel.fokus');
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->_table)->row_array();
	}

	function get($limit,$offset)
	{
		$this->db->limit($limit);
		$this->db->offset($offset);
		//$this->db->join('t_artikel', 't_fokus.id = t_artikel.fokus');
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->_table)->result_array();
	}

	function getjoin($limit,$offset)
	{
		$this->db->limit($limit);
		$this->db->offset($offset);
		//$this->db->join('t_artikel', 't_fokus.id = t_artikel.fokus');
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->_table)->result_array();
	}

	function getfokus2()
	{
		return $this->db->get('t_kolom', 100, 1)->result_array();
	}

	function updateartikel($data, $id)
	{
		$this->db->where('id_section', $id);
		$this->db->update('t_artikel', $data);
	}
}