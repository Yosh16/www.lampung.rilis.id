<script> 
   $(document).ready(function(){
       $("#flip").click(function(){
           $("#panel").slideToggle("slow");
       });
   });
   $(window).load(function(){
           $("#panel").slideUp("fast");
       });
</script>
<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<script>
 $(document).ready(function(){
     i=1;
     $('#more').click(function(){
       var data = $('.form-user3').serialize();
        i = i+3;
        // $('#result').html('');
        j = (i-1);

         var id = $('#filter').val();
       console.log(data);
        $('#more_text').val(j);
         $.ajax({
            url:'<?php echo site_url('search/more/');?>'+j+'/'+id,
            method:'get',
             data:data,
             dataType:'text',
             success:function(data)
             {
               $('#result').append(data);
             }
         });
      
     });
   });
 </script>
<section id="berita_pilihan_dan_populer_rilis kategori detail">
   <div class="container">
      <div class="row center__berita__pilihan">
         <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col_berita_pilihan_dan_populer_rilis">
            <div class="room_center_slider_headline">
               <!--content-->
               <div class="sub_menu_page_detail">
                  <div class="nama_sub0001"><a href="">HOME</a></div>
                  <div class="right_sub"><i class="fa fa-angle-double-right angle_0111"></i></div>
                  <div class="nama_sub0001"><a href="#">LIST TAG</a></div>
                  <div class="clear"></div>
               </div>
               <div class="pull-left select_0 select_komentar custom__indeks">
               </div>
               <div class="clear"></div>
               <div class="row berita__lainnya" id="result">
                  <?php 
                     $all = $this->T_artikel->list_tag($list['id_tag']);
                     foreach($all as $a){?>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 custom_fokus">
                      <div class="entry-item">
                        <?php  $path = date('Y/m/d/', strtotime($a['postdate']));
                           if(!empty($a['thumbnail'])) {
                             $mystring = $a['thumbnail'];
                             $findme   = 'http';
                             $pos = strpos($mystring, $findme);
                             if ($pos === false) {?>
                        <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$upload.$path.$a['thumbnail'];?>&w=800&h=496&cz=1" alt="berita" title="berita" height="496" width="800"> </a> </div>
                        <?php }else{ ?>
                        <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$a['thumbnail'];?>&w=800&h=496&cz=1" alt="berita" title="berita" height="496" width="800"> </a> </div>
                        <?php } ?>
                        <?php ;} else {?>
                        <div class="entry-thumb"> <a href="#"> <img class="lazy img_responsive_100" data-original="<?php echo $tim.$a['oldimage'];?>&w=800&h=496&cz=1" alt="berita" title="berita" height="496" width="800"> </a> </div>
                        <?php } ?>
                        <div class="entry-box">
                           <header class="entry-header">
                              <div class="entry-meta"> <span class="entry-date"><?php
                                 $ex = explode(' ', $a['tgl_pub']);
                                 $num = date('N', strtotime($ex[0]));
                                 $hari = array ( 1 =>    
                                   'Senin',
                                   'Selasa',
                                   'Rabu',
                                   'Kamis',
                                   'Jumat',
                                   'Sabtu',
                                   'Minggu'
                                 );
                                 echo $hari[$num];
                                 ?>, 
                                 <?php echo date('d/m/Y H.i', strtotime($a['tgl_pub']));?> </span>
                              </div>
                              <h4 class="entry-title st-4-1"> <a class="s41" href="<?php echo site_url($a['urltitle']);?>"><?php echo $a['judul_artikel'];?></a> </h4>
                              <a href="<?php echo site_url('tag').'/'.$list['nama_tag'];?>">
                                 <div class="tag__berita__laiinya">#<?php echo $list['nama_tag'];?></div>
                              </a>
                           </header>
                        </div>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="clear"></div>
                  <form class="form-user3">
                <input type="hidden" name="catparam" id="catparam" value="<?php echo $this->uri->segment(2);?>">
                <input type="hidden" name="more_text" id="more_text" value="9">
                </form>
               </div>
                  <div align="center">
                        <div class="center__more__berita__rilis" id="more">Lihat lebih banyak berita</div>
                  </div>
            </div>
         </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 detail-page-rilis" id="sidebarrightkategori">
         <div class="ads-iklan-800 pad0"><img src="<?php echo $images;?>iklan-dupont-pioneer-870x725.jpg" alt="iklan" title="iklan"/></div>
         <div class="widget-title st-2-4">
            <h3>POPULAR</h3>
            <div class="separator-6"></div>
         </div>
         <?php $i=1; foreach ($populer as $pop){?>
         <div class="clear"></div>
         <div class="room__berita__popular">
            <a href="<?php echo site_url(url_title($pop['urltitle']).'.html');?>">
               <div class="in__room__berita__popular">
                  <div class="pull-left no__populer__rilis"><?php echo $i; ?></div>
                  <div class="pull-left berita__populer__rilis"><?php echo $pop['judul_artikel']; ?></div>
                  <div class="clear"></div>
               </div>
            </a>
         </div>
         <?php $i++; } ?>
         <div class="tag_footer_div">
            <?php foreach($menu as $a){   
               $count =  count($a );?>
            <a href="<?php echo site_url('page/'.url_title($a['titleurl']));?>">
               <div class="left_t_footer_n01"><?php echo strtoupper($a['judul_hs']);?></div>
            </a>
            <?php } ?>
            <div class="clear"></div>
         </div>
         <!-- <div align="center">
            <div class="apps__center">
              <div class="pull-left apps"><img src="<?php //echo $images;?>google_play.png"></div>
              <div class="pull-left apps store"><img src="<?php //echo $images;?>appstore.png"></div>
              <div class="clear"></div>
            </div>
                    </div> -->
         <div align="center">
            <div class="social_media_footer">
               <a href="http://<?php echo $sosmed['facebook'];?>" target="_blank">
                  <div class="my_social"><i class="fa fa-facebook fb"></i></div>
               </a>
               <a href="http://<?php echo $sosmed['twitter'];?>" target="_blank">
                  <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
               </a>
               <a href="http://<?php echo $sosmed['instagram'];?>" target="_blank">
                  <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
               </a>
               <a href="http://<?php echo $sosmed['gplus'];?>" target="_blank">
                  <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
               </a>
               <div class="clear"></div>
            </div>
         </div>
      </div>
      <div class="clear"></div>
   </div>
   </div>
</section>
</body>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>