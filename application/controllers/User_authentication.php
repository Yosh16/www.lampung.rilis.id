<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_Authentication extends CI_Controller
{
    function __construct() {
		parent::__construct();
		
		// Load facebook library
		$this->load->library('facebook');
		
		//Load user model
		$this->load->model('T_user');
		$this->load->model('T_user_detail');
    }
    
    public function index(){
		$userData = array();
		
		// Check if user is logged in
		
			// Get user facebook profile details
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture,address,about');

            // Preparing data for database insertion
            /*
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['locale'] = $userProfile['locale'];
            $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];
            */

            print_r($userProfile);
			
            $cek = $this->T_user->ceksosmed($userProfile['id']);
            //print_r($cek);
            //die();
           	if(!empty($cek))
			{
				$data_session = array(
					'id'			=> 	$cek['id'],
					'nama_lengkap'	=> 	$cek['nama_lengkap']
								);
				$this->session->set_userdata($data_session);
				redirect('rilizen');
			}
			else
			{
				if(!empty($userProfile['email']))
				{
					$email = $userProfile['email'];
				}
				else
				{
					$email = '';
				}

				

				if(!empty($userProfile['address']))
				{
					$address = $userProfile['address'];
				}
				else
				{
					$address = '';
				}


				if(!empty($userProfile['about']))
				{
					$description = $userProfile['about'];
				}
				else
				{
					$description = '';
				}

				if(!empty($userProfile['locale']))
				{
					$region = $userProfile['locale'];
				}
				else
				{
					$region = '';
				}


				$user = array(
					'nama_lengkap'	=>	$userProfile['first_name'].' '.$userProfile['last_name'],
					'email'			=>	$email,
					'password'		=>	'',
					'profesi'		=>	'',
					'poin'			=>	'',
					'salt'			=>	'',
					'id_sosmed'		=>	$userProfile['id']);
				$this->T_user->add($user);
				$id = $this->db->insert_id();
				$userdetail = array(
					'user_id'		=>	$id,
					'phone'			=>	'',
					'image'			=>	'http://graph.facebook.com/'.$userProfile['id'].'/picture?type=large&redirect=true&width=200&height=200',
					'banner'		=>	'',
					'birthday'		=>	'0000-00-00',
					'place'			=>	$region,
					'facebook'		=>	'https://www.facebook.com/'.$userProfile['id'],
					'twitter'		=>	'',
					'gplus'			=>	'',
					'address'		=>	$address,
					'description'	=>	$description);
				$this->T_user_detail->add($userdetail);
				$data_session = array(
					'id'			=> 	$id,
					'nama_lengkap'	=> 	$userProfile['first_name'].' '.$userProfile['last_name']
								);
				$this->session->set_userdata($data_session);
				redirect('rilizen');
			}
			
            
    }

	public function logout() {
		// Remove local Facebook session
		$this->facebook->destroy_session();
		// Remove user data from session
		$this->session->unset_userdata('userData');
		// Redirect to login page
        redirect('/user_authentication');
    }
}
