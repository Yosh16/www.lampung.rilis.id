<?php include_once dirname(__FILE__).'/../layout/header.php';?>
<section id="kategori-page-rilis">
  <div class="container">
      <div class="row col-lg-9-3-detail-page">
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 detail-page-rilis">  
          <div class="room_center_slider_headline"> 
          <!--content-->
          <div class="sub_menu_page_detail">
            <!-- <?php
            if(!empty($detail['fokus'])) {?>

                <div class="nama_sub0001">
                  <a href="<?php echo site_url();?>">HOME</a>
                </div>
                <div class="right_sub">
                  <i class="fa fa-angle-double-right angle_0111"></i>
                </div>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url('kategori/fokus');?>">
                    FOKUS
                  </a>
                </div>
                <div class="clear"></div>

            <?php ;} else {?> -->

                <?php if(!empty($detail['id_supsection'])) {
                      $section = $this->T_section->sectionbread($detail['id_supsection']);
                ?>

                <?php
                if($section['id_section'] == 142 || $section['id_supsection'] == 142) {?>

                <div class="nama_sub0001">
                  <a href="<?php echo site_url();?>">HOME</a>
                </div>
                <div class="right_sub">
                  <i class="fa fa-angle-double-right angle_0111"></i>
                </div>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url('kategori/kolom');?>">
                    KOLOM                     
                  </a>
                </div>
                <div class="right_sub">
                  <i class="fa fa-angle-double-right angle_0111"></i>
                </div>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url('kategori/'.$detail['url_title']);?>">
                    <?php echo strtoupper($detail['nama_section']);?>                    
                  </a>
                </div>
                <div class="clear"></div>


                <?php ;} else {?>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url();?>">HOME</a>
                </div>
                <div class="right_sub">
                  <i class="fa fa-angle-double-right angle_0111"></i>
                </div>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url('kategori/'.$section['url_title']);?>">
                    <?php echo strtoupper($section['nama_section']);?>                      
                  </a>
                </div>
                <div class="right_sub">
                  <i class="fa fa-angle-double-right angle_0111"></i>
                </div>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url('kategori/'.$detail['url_title']);?>">
                    <?php echo strtoupper($detail['nama_section']);?>                    
                  </a>
                </div>
                <div class="clear"></div>
                <?php ;} ?>

                <?php ;} else {?>

                <div class="nama_sub0001">
                  <a href="<?php echo site_url();?>">HOME</a>
                </div>
                <div class="right_sub">
                  <i class="fa fa-angle-double-right angle_0111"></i>
                </div>
                <div class="nama_sub0001">
                  <a href="<?php echo site_url('kategori/'.$detail['url_title']);?>">
                    <?php echo strtoupper($detail['nama_section']);?>                    
                  </a>
                </div>
                <div class="clear"></div>


                <?php ;} ?>

            <!-- <?php ;} ?> -->
            <div class="clear"></div>
          </div>
          <div class="judul_berita22"><?php echo $detail['judul_artikel'];?></div>
          <div class="socmed_content_01">
            <div class="row">
            <?php
            if($detail['rilizen'] == 'N')
            {
              $getadmin = $this->T_admin->getid($detail['id_admin']);
              $admin = $getadmin['nama'];
              $adminid  = $getadmin['id_adm'];
              $pic = $tim.$mimin.$getadmin['foto_admin'];
            }
            else
            {
              $getadmin = $this->T_user->getid($detail['id_admin']);
              $admin = $getadmin['nama_lengkap'];
              $adminid  = $getadmin['user_id'];
               //$pic = $tim.$rilizen.$getadmin['image'];

              $mystring = $getadmin['image'];
              $findme   = 'http';
              $pos = strpos($mystring, $findme);
              if ($pos === false) 
              {
                $pic = $rilizen.$getadmin['image'];
              } 
              else 
              { 
                $pic = $getadmin['image'];
              }

            }?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 five_in_socmed">
                <div class="img_kontributor_01">
                  <?php
                  if($detail['rilizen'] == 'N') {?>
                  <a href="<?php echo site_url('penulis/'.$adminid);?>" class="penulis_gone_480">
                  <?php ;} else {?>
                    <?php
                      if(!empty($this->session->userdata('nama_lengkap'))) {?>
                        <a href="<?php echo site_url('rilizen');?>" class="penulis_gone_480">
                      <?php ;} else {?>
                        <a href="<?php echo site_url('rilizen/user/'.$adminid);?>" class="penulis_gone_480">
                      <?php ;} ?>
                  <?php ;} ?>  
                  <img src="<?php echo $pic;?>" title="konributor" alt="kontributor" class="kontributor_img_0" height="50" width="50"> 
                  </a>

                  <img src="<?php echo $images;?>sticky_logo.png" title="konributor" alt="kontributor" class="kontributor_img_0 admin" width="28" height="40"> </div>
                <div class="room_post_name_in_date_kontributor">
                   <?php
                    if($detail['rilizen'] == 'N') {?>
                      <a href="<?php echo site_url('penulis/'.$adminid);?>" class="penulis_gone_480"><?php echo $admin;?></a><br>
                    <?php ;} else {?>
                      <?php
                        if(!empty($this->session->userdata('nama_lengkap'))) {?>
                          <a href="<?php echo site_url('rilizen');?>" class="penulis_gone_480"><?php echo $admin;?></a><br>
                        <?php ;} else {?>
                          <a href="<?php echo site_url('rilizen/user/'.$adminid);?>" class="penulis_gone_480"><?php echo $admin;?></a><br>
                        <?php ;} ?>
                    <?php ;} ?> 
                    <span><?php
                      $ex = explode(' ', $detail['tgl_pub']);
                      $num = date('N', strtotime($ex[0]));
                      $hari = array ( 1 =>    
                        'Senin',
                        'Selasa',
                        'Rabu',
                        'Kamis',
                        'Jumat',
                        'Sabtu',
                        'Minggu'
                      );
                      echo $hari[$num];
                      ?>, <?php echo date('Y/m/d H.i', strtotime($detail['tgl_pub']));?></div>
                <div class="clear"></div>
            </div>
            <!--div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 seven_in_socmed">
                <div align="center">
                    <div class="room_total_101001 no_pad_right">
                        <div class="jumlah_room_shared">10 K</div>
                        <div class="title_room_shared">KOMENTAR</div>
                    </div>
                    <div class="room_total_101001">
                        <div class="jumlah_room_shared">10 K</div>
                        <div class="title_room_shared">SHARE</div>
                    </div>

                    <div class="clear"></div>
                </div>
            </div-->
        </div>
          </div>
          <?php $path = date('Y/m/d/', strtotime($detail['postdate']));
           if(!empty($detail['thumbnail_watermark'])) {?>
            <div class="img_big_berita ovh__rilis"><img src="<?php echo $tim.$detail['thumbnail_watermark'];?>&w=1200" class="lazy img100" title="berita" alt="berita"></div>
            <?php } elseif(!empty($detail['thumbnail'])) {
            $mystring = $detail['thumbnail'];
            $findme   = 'http';
            $pos = strpos($mystring, $findme);
            if ($pos === false) {?>
          <div class="img_big_berita ovh__rilis"><img src="<?php echo $tim.$upload.$path.$detail['thumbnail'];?>&w=1200" class="lazy img100" title="berita" alt="berita"></div>
            <?php }else{ ?>
          <div class="img_big_berita ovh__rilis"><img src="<?php echo $tim.$detail['thumbnail'];?>&w=1200" class="lazy img100" title="berita" alt="berita"></div>
            <?php }?>
          <?php ;} else {?>
          <div class="img_big_berita ovh__rilis"><img src="<?php echo $tim.$detail['oldimage'];?>&w=1200" class="lazy img100" title="berita" alt="berita"></div>
            <?php ;}?>
          <div class="sub-berita_post"><?php echo $detail['ket_thumbnail'];?></div>
          <div class="aside_detail_berita">
            <?php $explode = explode("<p>", $detail['isi_artikel']); 
              $count = (int)count($explode) - (int)1;
             // $jumlah = ceil($count);?>


              <?php
              $i = 0;
              foreach ($explode as $explode) {
                if($i == 0){ ?>
                  <span><?php if(empty($detail['oldid'])) { ?>

                     <?php
                if($section['id_section'] == 143 || $section['id_supsection'] == 143) { }else{ ?>

                    RILIS.ID, <?php echo $detail['kota'];?>

                    <?php } ?>

                 </span><?php }else{'';} ?></span></span><?php echo $explode; ?>
                <?php } elseif($i == 3) { ?>
                  <div class="baca-juga" style="background-color: #fff">
                    <p class="baca-juga__header">Baca Juga</p>
                    <ul class="baca-juga__list">
                    <?php foreach($terkait as $a){ ?>
                      <li><a target="_blank" href="<?php echo site_url($a['urltitle']).'.html';?>"><?php echo $a['judul_artikel'];?></a></li>
                    <?php } ?>
                    </ul>
                  </div><?php echo $explode; ?>
                <?php } else {
                  echo $explode;
                }
              $i++;}
              ?>
               <?php
                if(!empty($detail['editor'])) {?>
                <?php $editor = $this->T_admin->getid($detail['editor']);?>
                <p style="font-size: 13px"><b>Editor</b> <?php echo $editor['nama'];?></p>
                <?php ;} else {'';}?>
                <?php if(!empty($detail['url'])) {?>
                <p style="font-size: 13px"><b>Sumber</b> <a href="<?php echo $detail['url'];?>" target="__blank"><?php echo $detail['url'];?></a></p>
                <?php ;} else {'';}?>
                <div class="ads__top pizza" style="background:url(<?php echo $images;?>201412820203161d0.jpg);"></div>
          </div>
          <div class="sharethis-inline-share-buttons"></div><br>
          <!-- <div align="center">
            <div class="room_share_down">
              <div class="pull-left text-left-share-down">Bagikan artikel</div>
              <div class="pull-left left-button-share">
                <div class="socmed_shared"><a href=""><img data-original="<?php echo $images;?>Facebook.png" alt="facebook rilis" title="facebook rilis" class="img_socmed_128" width="128" height="128"></a></div>
                <div class="socmed_shared"><a href=""><img data-original="<?php echo $images;?>twitter.png" alt="twitter rilis" title="twitter rilis" class="img_socmed_128" width="128" height="128"></a></div>
                <div class="socmed_shared"><a href=""><img data-original="<?php echo $images;?>google_plus.png" alt="google_plus rilis" title="google_plus rilis" class="img_socmed_128" width="128" height="128"></a></div>
                <div class="socmed_shared"><a href=""><img data-original="<?php echo $images;?>Whatsaap.png" alt="Whatsaap rilis" title="Whatsaap rilis" class="img_socmed_128" width="128" height="128"></a></div>
                <div class="socmed_shared"><a href=""><img data-original="<?php echo $images;?>line.png" alt="line rilis" title="line rilis" class="img_socmed_128" width="128" height="128"></a></div>
              </div>
              <div class="clear"></div>
            </div>
          </div> -->
          <div class="kolom_tags">
            <div class="tags_01">
              <div class="text_tags">Tags</div>
              <div class="fatags"><i class="fa fa-tags tags23"></i></div>
            </div>
            <?php foreach($tags as $a){ ?>
            <a href="<?php echo site_url('tag').'/'.$a['seo_tag'];?>">
            <div class="mini-tags">#<?php echo $a['seo_tag'];?></div>
            </a>
            <?php } ?>
            <div class="clear"></div>
          </div>
         <div class="tile_07655">Bagaimana reaksi anda tentang artikel ini?</div>
            
          <div align="center">
            <div class="room_emoticon_002">
            <div class="row emoticon">
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002"> 
                  
                  
                  <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'suka')" id="suka" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="suka">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>
                  

                  <div class="img-emoticon"><img data-original="<?php echo $images;?>icon_suka.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Suka</div>
                    <div class="room_persen" id="sukaresult">
                      0%
                    </div>
                  </a> 
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002"> 


                  <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'sedih')" id="sedih" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="sedih">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>


                  <div class="img-emoticon"><img data-original="<?php echo $images;?>icon_sedih.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Sedih</div>
                  <div class="room_persen" id="sedihresult">
                    0%
                  </div>
                  </a> </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002">

                 <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'tidak_suka')" id="tidak_suka" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="tidak_suka">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>

                  
                  <div class="img-emoticon"><img data-original="<?php echo $images;?>icon-tidaksuka.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Tidak suka</div>
                  <div class="room_persen" id="tidak_sukaresult">
                    0%
                  </div>
                  </a> </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002">

                 <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'terkejut')" id="terkejut" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="terkejut">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>

                  <div class="img-emoticon"><img data-original="<?php echo $images;?>icon_kaget.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Tekejut</div>
                  <div class="room_persen" id="terkejutresult">
                    0%
                  </div>
                  </a> </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002">

                 <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'bosen')" id="bosen" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="bosen">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>
                  
                  <div class="img-emoticon"><img data-original="<?php echo $images;?>iconbosen.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Bosen</div>
                  <div class="room_persen" id="bosenresult">
                    0%
                  </div>
                  </a> </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002">

                  <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'bingung')" id="bingung" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="bingung">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>
                  
                  <div class="img-emoticon"><img data-original="<?php echo $images;?>bingung.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Bingung</div>
                  <div class="room_persen" id="bingungresult">
                   0%
                  </div>
                  </a> </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 two_reaction">
                <div class="room_emoticon_002">

                  <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                  <a onclick="react(<?php echo $detail['id_artikel'];?>, 'marah')" id="marah" data-id="<?php echo $detail['id_artikel'];?>" data-reaksi="marah">
                  <?php ;} else { ?>
                  <a href="<?php echo site_url('login/index/'.$detail['id_artikel'].'-auth');?>">
                  <?php ;} ?>
                  
                  <div class="img-emoticon"><img data-original="<?php echo $images;?>marah.png" class="emoticon_0" alt="emotiocn rilis" title="emoticon_rilis" height="256" width="256"></div>
                  <div class="tanggapan">Marah</div>
                  <div class="room_persen" id="marahresult">
                    0%
                  </div>
                  </a> </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          </div>
          <div class="pop_blocks_text_area">
            <form action="<?php echo site_url('komentar/addaction');?>" method="POST">
              <div class="blocks_in_text_area_comment_account">
                <div class="right_blocks_area">Komentar</div>
                <div class="clear"></div>
              </div>
              
              <div class="box_text_area_comment">
                <textarea name="komentar" class="text_area_0000" placeholder="Isi Komentar Anda" required maxlength="500" id="komentar"></textarea>
              </div>
              <div class="box_ededed">
                <div class="fl_500_01" id="countkomentar">500</div>
                <div class="for_right_000">
                  <input type="hidden" name="id" value="<?php echo $detail['id_artikel'];?>">
                  <button type="submit" class="post_comment_000">post comment</button>
                </div>
                <div class="clear"></div>
              </div>
            </form>
          </div>
          <div class="room_penulis">
            <div class="comment_to_detail0">
              <div class="fl_comment_to_001">komentar (<?php echo count($komentarall);?>)</div>
              <div class="clear"></div>
            </div>
            <?php
            function tanggal_indo($tanggal, $cetak_hari = false)
            {
              $hari = array ( 1 =>    'Senin',
                    'Selasa',
                    'Rabu',
                    'Kamis',
                    'Jumat',
                    'Sabtu',
                    'Minggu'
                  );
                  
              $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                  );
              $split    = explode('-', $tanggal);
              $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
              
              if ($cetak_hari) {
                $num = date('N', strtotime($tanggal));
                return $hari[$num] . ', ' . $tgl_indo;
              }
              return $tgl_indo;
            }
            foreach($komentar as $komentar) {
              $tanggal = explode(' ', $komentar['postdate']);
              ?>
            <div class="item_k">
              <?php
              if(!empty($this->session->userdata('nama_lengkap'))) {?>
                <a class="name" style="text-decoration:none;" href="<?php echo site_url('rilizen')?>">
              <?php ;} else {?>
                <a class="name" style="text-decoration:none;" href="<?php echo site_url('rilizen/user/'.$komentar['user_id'])?>">
              <?php ;} ?>
                <img data-original="<?php echo $rilizen.$komentar['image'];?>">
              </a>
              <p class="message"> 
                 <?php
                  if(!empty($this->session->userdata('nama_lengkap'))) {?>
                    <a class="name" style="text-decoration:none;" href="<?php echo site_url('rilizen')?>">
                  <?php ;} else {?>
                    <a class="name" style="text-decoration:none;" href="<?php echo site_url('rilizen/user/'.$komentar['user_id'])?>">
                  <?php ;} ?>
                  <?php echo $komentar['nama_lengkap'];?></a> <small class="text-muted"><i class="fa fa-clock-o"></i> <?php echo tanggal_indo ($tanggal[0], true);?> | <?php echo substr($tanggal[1],0, 5);?></small> <br>
                <?php echo $komentar['komentar'];?></p>
              <div class="in_comment_0965444"> <!--a onClick="show_laporkan('AVu_9xCnmJZlWbjFFdYp','timotius wibowop')" style="cursor:pointer;" id="a_laporkan_AVu_9xCnmJZlWbjFFdYp"><i class="fa fa-exclamation-circle"></i> Laporkan</a--> </div>
            </div>
            <?php ;} ?>
            <div align="center">
              <?php if(count($komentarall) >= 3){ ?>
              <div class="post_komentar_88967858"> <a href="<?php echo site_url('komentar/'.$detail['urltitle'])?>"> 
                  <div class="left btn2 see">Lihat Semua Komentar</div>
                </a>
                <div class="clear"></div>
              </div>
              <?php } else {'';} ?>
            </div>
          </div>
          <div class="row news__lainnya__berita custom__again__rilis">
              <div class="col-lg-6 col-md-6 col-sm-6 six__berita__lainnya detail__custom__again">
                  <div class="ro0m___article___title_______line"> <a href="#terkait-1" class="box-title red-500 current">Terkait</a> </div>
                  <div class="sub__parent__kategori__focus">
                      <div class="post___in___parents__two">
                <?php foreach($terkait as $a){ ?>
                  <div class="row parents__sub__kategori">
                  <?php  $path = "images/".date('Y/m/d/', strtotime($a['postdate']));
                    if(!empty($a['thumbnail'])) {
                      $mystring = $a['thumbnail'];
                      $findme   = 'http';
                      $pos = strpos($mystring, $findme);
                      if ($pos === false) {?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                      <div class="ovh__rilis image__relative__focus detail"><img data-original="<?php echo $tim.$upload.$path.$a['thumbnail'];?>&w=854&h=480&cz=1" alt="focus" title="focus" class="lazy img100 relative__focus"></div>
                      </a> </div>
                    <?php }else{ ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                      <div class="ovh__rilis image__relative__focus detail"><img data-original="<?php echo $tim.$a['thumbnail'];?>&w=854&h=480&cz=1" alt="focus" title="focus" class="lazy img100 relative__focus"></div>
                      </a> </div>
                    <?php }?>
                    <?php ;} else {?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                      <div class="ovh__rilis image__relative__focus detail"><img data-original="<?php echo $tim.$a['oldimage'];?>&w=854&h=480&cz=1" alt="focus" title="focus" class="lazy img100 relative__focus"></div>
                      </a> </div>
                    <?php ;} ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                      <div class="judul__two__focus__kategori"><?php echo $a['judul_artikel'];?></div>
                      </a>
                      <div class="time__-date__two__kategori__focu__rilis"><?php
                    $ex = explode(' ', $a['tgl_pub']);
                    $num = date('N', strtotime($ex[0]));
                    $hari = array ( 1 =>    
                      'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    echo $hari[$num];
                    ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?></div>
                      <a href="<?php echo site_url('kategori/'.url_title($a['url_title']))?>">
                      <div class="tags___berita__detail___rilis"><?php echo $a['nama_section'];?></div>
                      </a> </div>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                </div>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 six__berita__lainnya detail__custom__again">
                  <div class="ro0m___article___title_______line"> <a href="#terkait-1" class="box-title red-500 current">Terkini</a> </div>
                  <div class="sub__parent__kategori__focus">
                      <div class="post___in___parents__two">
                          <?php foreach($recent as $a){ ?>
                            <div class="row parents__sub__kategori">
                            <?php  $path = "images/".date('Y/m/d/', strtotime($a['postdate']));
                              if(!empty($a['thumbnail'])) {
                                $mystring = $a['thumbnail'];
                                $findme   = 'http';
                                $pos = strpos($mystring, $findme);
                                if ($pos === false) {?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                                <div class="ovh__rilis image__relative__focus detail"><img data-original="<?php echo $tim.$upload.$path.$a['thumbnail'];?>&w=854&h=480&cz=1" alt="focus" title="focus" class="lazy img100 relative__focus"></div>
                                </a> </div>
                                <?php }else{ ?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                                <div class="ovh__rilis image__relative__focus detail"><img data-original="<?php echo $tim.$a['thumbnail'];?>&w=854&h=480&cz=1" alt="focus" title="focus" class="lazy img100 relative__focus"></div>
                                </a> </div>
                                <?php }?>
                                <?php ;} else {?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                                <div class="ovh__rilis image__relative__focus detail"><img data-original="<?php echo $tim.$a['oldimage'];?>&w=854&h=480&cz=1" alt="focus" title="focus" class="lazy img100 relative__focus"></div>
                                </a> </div>
                                <?php ;} ?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 parents__sub__kategori detail"> <a href="<?php echo site_url(url_title($a['urltitle']).'.html')?>">
                                <div class="judul__two__focus__kategori"><?php echo $a['judul_artikel'];?></div>
                                </a>
                                <div class="time__-date__two__kategori__focu__rilis"><?php
                              $ex = explode(' ', $a['tgl_pub']);
                              $num = date('N', strtotime($ex[0]));
                              $hari = array ( 1 =>    
                                'Senin',
                                'Selasa',
                                'Rabu',
                                'Kamis',
                                'Jumat',
                                'Sabtu',
                                'Minggu'
                              );
                              echo $hari[$num];
                              ?>, <?php echo date('Y/m/d H.i', strtotime($a['tgl_pub']));?></div>
                                <a href="<?php echo site_url('kategori/'.url_title($a['url_title']))?>">
                                <div class="tags___berita__detail___rilis"><?php echo $a['nama_section'];?></div>
                                </a> </div>
                              <div class="clear"></div>
                            </div>
                            <?php } ?>
                          </div>
                  </div>
              </div>
          </div>
          </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 detail-page-rilis" id="sidebarrightkategori">
              <div class="ads-iklan-800 pad0"><img src="<?php echo $images;?>iklan-dupont-pioneer-870x725.jpg" alt="iklan" title="iklan" /></div>
                    <div class="widget-title st-2-4">
                        <h3>POPULAR</h3>
                        <div class="separator-6"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="room__berita__popular">
                        <?php
                          $i=1;
                         foreach ($populer as $pop){?>
                        <a href="<?php echo site_url(url_title($pop['urltitle']).'.html');?>">
                            <div class="in__room__berita__popular">
                                <div class="pull-left no__populer__rilis"><?php echo $i; ?></div>
                                <div class="pull-left berita__populer__rilis"><?php echo $pop['judul_artikel']; ?></div>
                                <div class="clear"></div>
                            </div>
                        </a>
                        <?php $i++; } ?>
                    </div>
              <div class="tag_footer_div"> 
                    <?php $static = $this->T_halamanstatis->select();
                    $count =  count($static );
                    $i=1;
                    foreach($static as $a){ ?>
                <a target="_blank" href="<?php echo site_url('page/'.url_title($a['titleurl']));?>">
                  <div class="left_t_footer_n01"><?php echo strtoupper($a['judul_hs']);?></div>
                </a>
                <?php } ?>
              
                <div class="clear"></div>
              </div>
<!--               <div align="center">
    <div class="apps__center">
        <div class="pull-left apps"><img src="<?php //echo $images;?>google_play.png"></div>
        <div class="pull-left apps store"><img src="<?php //echo $images;?>appstore.png"></div>
        <div class="clear"></div>
    </div>
</div> -->
              <div align="center">
                <?php 
                $sosmed   = $this->T_informasi->get();?>
                  <div class="social_media_footer"> <a href="http://<?php echo $sosmed['facebook'];?>" target="_blank">
                    <div class="my_social"><i class="fa fa-facebook fb"></i></div>
                    </a> <a href="http://<?php echo $sosmed['twitter'];?>" target="_blank">
                    <div class="my_social"><i class="fa fa-twitter twitter"></i></div>
                    </a> <a href="http://<?php echo $sosmed['instagram'];?>" target="_blank">
                    <div class="my_social"><i class="fa fa-instagram instagram"></i></div>
                    </a> <a href="http://<?php echo $sosmed['gplus'];?>" target="_blank">
                    <div class="my_social menu_footer_none"><i class="fa fa-google-plus gplus"></i></div>
                    </a>
                    <div class="clear"></div>
                  </div>
                </div>

          </div>
          <div class="clear"></div>
        </div>
    </div>
</section>
<?php include_once dirname(__FILE__).'/../layout/footer.php';?>