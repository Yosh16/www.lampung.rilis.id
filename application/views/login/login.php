<?php 
$base               = $this->config->item('base_url');
$css                = $this->config->item('css');
$images             = $this->config->item('images');
$font               = $this->config->item('font');
$js                 = $this->config->item('js');
$fonts              = $this->config->item('fonts');
$images_upload      = $this->config->item('images_front');
$upload_images      = $this->config->item('upload_images');
$tim                = $this->config->item('images_tim');
$images_upload      = $this->config->item('images_upload');
?>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="keywords" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="author" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta property="og:title" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam"/>
<meta property="og:image" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam"/>
<meta property="og:url" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam"/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="twitter:image" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="twitter:url" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<meta name="twitter:card" content="RILIS.ID Situs Berita Politik Indonesia Menyajikan Berita Politik Terkini dan Terbaru Seputar Peristiwa Politik Nasional, Dunia, Daerah, Elektoral, Inspirasi, Bisnis, Muda, Trend dan Ragam" />
<link rel="shortcut icon" type="image/png" href="<?php echo $images ;?>favicon.png"/>
<link rel="shortcut icon" type="image/png" href="<?php echo $images ;?>favicon.png"/>
<link rel="stylesheet" type="text/css" href="<?php echo $fonts;?>font-awesome.min.css"/>
<link rel="stylesheet" href="<?php echo $css ;?>bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $css ;?>owl.carousel.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $css ;?>fancybox.min.css"/>
<link rel="stylesheet" href="<?php echo $css ;?>rilis.css"/>
</head>
<body>

    <div class="big__large__content__rilis">
        <div class="bg-cover-login"></div>
        <div class="aside-left-login">
            <div class="panel-body-login">
                <div align="center" class="image___login"><a href="<?php echo site_url('home');?>"><img src="<?php echo $images ;?>rilis-beta.png"/></a></div>
                <div class="text-first-body-login">Temukan beragam informasi politik, topik dan orang-orang yang menginspirasi kamu melalui rilis</div>
                
                <div class="form__socmed__login">
                    <form>
                        <div align="center">
                        <div class="pos__socmed__login">
                            <a onclick="location.href='<?php echo $authUrl;?>'" class="btn btn-block btn-lg btn-fb"><i class="fa fa-facebook-official sclogin"></i> Sign In with Facebook</a>
                        </div>
                        <div class="pos__socmed__login">
                            <a onclick="location.href='<?php echo site_url('hauth/login/Google');?>'" class="btn btn-block btn-lg btn-go"><i class="fa fa-google-plus-official sclogin"></i> Sign In with Gmail</a>
                        </div>
                        <div class="pos__socmed__login">
                            <a onclick="location.href='<?php echo site_url('hauth/login/Twitter');?>'" class="btn btn-block btn-lg btn-tw"><i class="fa fa-twitter sclogin"></i> Sign In with Twitter</a>
                        </div>
                        </div>
                    </form>

                    <div class="divide_cid"><span>Atau</span></div>
                    <form action="<?php echo site_url('login/login');?>" method="POST">
                        <div class="pos__login__input__rilis">
                            <input type="email" name="email" class="input__login__rilis" placeholder="Tulis email anda" required/>
                        </div>
                        <div class="pos__login__input__rilis">
                            <input type="password" name="password" class="input__login__rilis" placeholder="Password anda" required/>
                        </div>
                        <div align="center">
                            <?php if(!empty($id)) { ?> <input type="hidden" name="id" value="<?php echo $id;?>"> <?php ;} else {'';}?>
                            <div class="center__login__btn__rilis"><button class="btn-rilis-login" type="submit">Masuk</button></div>
                            <p style="color: #000; font-weight: bolder;">
                            <?php if(!empty($this->session->flashdata('errorlog'))) {echo $this->session->flashdata('errorlog');} else {'';}?>
                            </p>
                        </div>
                    </form>
                    <div class="pos__two__login">
                        <a href="<?php echo site_url('daftar');?>"><div class="pull-left text__bottom__login__rilis">Daftar Akun <span>RILIS</span></div></a>
                        <a href="forget.html"><div class="pull-right text__bottom__login__rilis">Lupa Password</div></a>
                        <div class="clear"></div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

</body>
<script src="<?php echo $js ;?>jquery.min.js"></script>
<script src="<?php echo $js ;?>bootstrap.min.js"></script>
<script src="<?php echo $js ;?>jquery.lazyload.min.js"></script>
<script src="<?php echo $js ;?>owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo $js ;?>fancybox.min.js"></script>
<script src="<?php echo $js ;?>rilis.js"></script>
</html>
